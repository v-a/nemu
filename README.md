# NEmu

## Introduction
NEmu (**Network Emulator for Mobile Universes**) is a distributed virtual network environment which fully runs without any administrative rights. It manages fleets of [QEMU](https://www.qemu.org/) VMs in order to build a dynamic virtual topology. It provides self-script (launcher and interpreter) and a python API to create and manage heterogeneous, dynamic, distributed, collaborative and mobile virtual networks.

**This branch of NEmu is compliant with QEMU and Python both from version 3.0**.<br/>
Support for **QEMU v2** can be found on branch **support/qemu2**.<br/>
Support for **Python v2** can be found on branch **support/python2**.

## Example
Let us consider the following network:

![usecase](https://gitlab.com/v-a/nemu/wikis/images/usecase.png)

The configuration of this network in NEmu would be:
```python
InitNemu() # Creates a new session

VSwitch('switch', niface=3) # New switch with 3 ports
VSlirp('slirp') # New external connection (Internet)

VHost('a', hds=[VFs('debian.img')], nics=[VNic()]) # New host with 1 NIC
VHost('b', hds=[VFs('windows.img')], nics=[VNic()]) # New host with 1 NIC
VHost('c', hds=[VFs('freebsd.img')], nics=[VNic(), VNic()]) # New host with 2 NICs

Link('a', 'switch:0') # Connects a to switch interface 0
Link('b', 'switch:1') # Connects b to switch interface 1
Link('c:0', 'switch:2') # Connects c to switch interface 2
Link('c:1', 'slirp') # Connects c to the real world

StartNemu() # Starts the virtual network
```

## For Who
NEmu is designed for anybody who wants to run native applications on a customized network without having access to a physical wide distributed infrastructure. NEmu can be useful for many purposes:
* Network application developer
* Cloud application developer (IaaS, PaaS or SaaS)
* Network course in school or university
* User wanting to test applications without impacting their real system
* Live network presentation
* Community volatile network testbed
* Pen-testing environment
* and much more!

## Documentation
Full documentation is available right [here](https://gitlab.com/v-a/nemu/wikis/).

## Licence
NEmu is a free software under the [LGPL v3 License](http://www.gnu.org/licenses/lgpl.html).
NEmu is written and maintained by Vincent Autefage.

NEmu uses some third party components in order to build virtual networks:

* Virtual links are emulated by a program called [vnd](https://www.labri.fr/perso/magoni/vnd) designed and written for NEmu by Pr. Damien Magoni.
* Mobility Model Scheduler is emulated by a program called [nemo](https://www.labri.fr/perso/magoni/nemo) designed and written for NEmu by Pr. Damien Magoni.
* Virtual containers run upon [boot2container](https://gitlab.freedesktop.org/gfx-ci/boot2container).
* Virtual routers run a custom version of the Linux distribution [TinyCore](http://tinycorelinux.net).
* Virtual nodes run upon [QEMU](https://qemu.org).

## References
* [Network Emulator: a Network Virtualization Testbed for Overlay Experimentations](https://ieeexplore.ieee.org/document/6335347) by Vincent Autefage & Damien Magoni in *International Workshop on Computer Aided Modeling and Design of Communication Links and Networks (CAMAD 17th)*. IEEE, September, 2012.
* [Virtualisation de réseau avec Network Emulator et application à l’évaluation d’un réseau recouvrant](https://hal.archives-ouvertes.fr/hal-00739177) by Vincent Autefage & Damien Magoni
in *Nouvelles Technologies de la Répartition et Ingénierie des Protcoles (NOTERE 11th / CFIP 15th)* **Best Paper**. CÉPADUÈS, October, 2012.
* [Virtualisation distribuée de réseaux dynamiques et mobiles avec NEmu](https://hal.archives-ouvertes.fr/hal-00823701) by Vincent Autefage & Damien Magoni in *Ingénierie des protocoles et Nouvelles technologies de la répartition : sélection d’articles de CFIP/NOTERE 2012*. RNTI-SM-2, HERMANN-RNTI, May, 2013.
* [NEmu : un outil de virtualisation de réseaux à la demande pour l’enseignement](https://hal.archives-ouvertes.fr/hal-00917673) by Vincent Autefage & Damien Magoni in *Journées RESeaux de l'Enseignement et de la Recherche (JRES 10th)*. RENATER, December, 2013.
* [NEmu: A distributed testbed for the virtualization of dynamic, fixed and mobile networks](https://www.sciencedirect.com/science/article/pii/S0140366416000281) by Vincent Autefage & Damien Magoni in *Computer Communications, volume 80*. Elsevier, April, 2016.
* [Virtualization Toolset for Emulating Mobile Devices and Networks](https://ieeexplore.ieee.org/document/7832990) by Vincent Autefage, Damien Magoni & John Murphy in *IEEE/ACM International Conference on Mobile Software Engineering and Systems (IEEE/ACM MobileSoft 3rd)*. IEEE/ACM, May, 2016.
