# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

'''This module contains functions to improve network port manipulations'''

import random
import psutil

def used_tcp_port(port):
    for conn in psutil.net_connections('tcp'):
        if conn.laddr[1] == port:
            return True
    return False

def used_udp_port(port):
    for conn in psutil.net_connections('udp'):
        if conn.laddr[1] == port:
            return True
    return False

def new_tcp_port():
    ret = 0
    while ret == 0 or used_tcp_port(ret):
        ret = random.randint(1025, 65535)
    return ret

def new_udp_port():
    ret = 0
    while ret == 0 or used_udp_port(ret):
        ret = random.randint(1025, 65535)
    return ret
