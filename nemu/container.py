# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

'''This module manages VContainer Containers'''

import os

from nemu.error import NemuError
from nemu.msg import (printc, printstr)

class Container:
    '''Container(image, cmd, **kargs)
    - EXPERIMENTAL FEATURE -
    - Def -
        A Container is a configuration item for VContainers.
    - Args -
        . image [str] --> Container image reference
        . cmd [str] --> Container command line arguments (Default: empty)
        . kargs [dict] --> other container engine long options. a=1, b_c=None, d=["y","z"] -> podman run --a=1 --b-c --d=y --d=z (Default: empty)
    '''
    def __init__(self, image, cmd=None, **kargs):
        self.image = str(image)
        self.cmd = cmd
        self.kargs = dict()
        for k in list(kargs.keys()):
            self.kargs[k.replace('_', '-')] = kargs[k]
        printc(str(self.image) + ' registered', 'cyan')

    def __str__(self):
        com = self.__class__.__name__ + '('
        com += 'image=' + printstr(self.image)
        com += ',cmd=' + printstr(self.cmd)
        com += ',**' + printstr(self.kargs)
        com += ')'
        return com

    def qstr(self):
        com = ''
        if self.kargs:
            keys = sorted(self.kargs.keys())
            for i in keys:
                if isinstance(self.kargs[i], list):
                    for j in self.kargs[i]:
                        com += '--' + str(i) + '=' + str(j) + ' '
                else:
                    com += '--' + str(i)
                    if not self.kargs[i] == None:
                        com += "=" + str(self.kargs[i])
                    com += ' '
        com += str(self.image)
        if self.cmd:
            com += ' ' + str(self.cmd)
        return com

    def __repr__(self):
        return str(self)

class ContainerNetwork:
    '''ContainerNetwork(iface, addr, gateway, forward, dns, routes)
    - EXPERIMENTAL FEATURE -
    - Def -
        A ContainerNetwork is a network configuration item for VContainers.
    - Args -
        . iface [int] -> Container network target interface
        . addr [str] --> IP address CIDR formatted or auto for DHCP (default: auto)
        . gateway [str] --> network gateway (Default: None)
        . routes [list of str] --> list of network routes as ["<destination>:<target>"...] (Default: empty)
        . dns [list of str] --> list of name servers (Default: empty)
        . forward [bool] --> enables IP forwarding (Default: False)
    '''
    def __init__(self, iface, addr="auto", gateway=None, routes=list(), dns=list(), forward=False):
        self.iface = iface
        self.addr = addr
        self.gateway = gateway
        self.forward = bool(forward)
        self.dns = list()
        self.routes = list()
        if not isinstance(self.iface, int):
            raise NemuError(str(self.iface) + ' is not an integer')
        self.dns.extend(dns)
        self.routes.extend(routes)

    def __str__(self):
        com = self.__class__.__name__ + '('
        com += 'iface=' + printstr(self.iface)
        com += ',addr=' + printstr(self.addr)
        com += ',gateway=' + printstr(self.gateway)
        com += ',routes=['
        for i in self.routes:
            com += printstr(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ',dns=['
        for i in self.dns:
            com += printstr(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ',forward=' + printstr(self.forward)
        com += ')'
        return com

    def qstr(self):
        com = 'b2c.iface=eth' + str(self.iface)
        if self.addr:
            if self.addr == 'auto':
                com += ',auto'
            else:
                com += ',address=' + str(self.addr)
        if self.gateway:
            com += ',gateway=' + str(self.gateway)
        if self.dns:
            for i in self.dns:
                com += ',nameserver=' + str(i)
        if self.routes:
            for i in self.routes:
                com += ',route=' + str(i)
        if self.forward:
            com += ',forward'
        return com

    def __repr__(self):
        return str(self)

class ContainerFS:
    '''ContainerFS(source, type, volume, flags, loads)
    - EXPERIMENTAL FEATURE -
    - Def -
        A ContainerFS is a filesystem configuration item for VContainers.
    - Args -
        . source [str] --> filesystem source path, address or identifier
        . type [str] --> filesystem type, e.g. ext4, virtio (Default: None)
        . volume [str] -> filesystem target volume tag (Default: basename of source)
        . flags [list of str] --> list of optional mounting options (Default: empty)
        . loads [list of str] --> list of optional OCI archive file loads located into volume (Default: empty)
    '''
    def __init__(self, source, type=None, volume=None, flags=list(), loads=list()):
        self.source = source
        self.type = type
        self.volume = volume
        self.flags = list()
        self.flags.extend(flags)
        self.loads = list()
        self.loads.extend(loads)
        if not self.volume:
            self.volume = os.path.basename(self.source)

    def __str__(self):
        com = self.__class__.__name__ + '('
        com += 'source=' + printstr(self.source)
        com += ',type=' + printstr(self.type)
        com += ',volume=' + printstr(self.volume)
        com += ',flags=['
        for i in self.flags:
            com += printstr(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ',loads=['
        for i in self.loads:
            com += printstr(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ')'
        return com

    def qstr(self):
        com = 'b2c.filesystem=' + str(self.volume)
        if self.type:
            com += ',type=' + str(self.type)
        com += ',src=' + str(self.source)
        com += ',opts=auto'
        if self.flags:
            for i in self.flags:
                com += '|' + str(i)
        com += ' b2c.volume=' + str(self.volume)
        com += ",filesystem=" + str(self.volume)
        if self.loads:
            for i in self.loads:
                com += ' b2c.load=' + str(self.volume) + '/' + str(i)
        return com

    def __repr__(self):
        return str(self)
