# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

'''This module contains functions to perform a node snapshot'''

from nemu.error import NemuError
from nemu.msg import printc
from nemu.vnode import VNode


def CreateVNodeSnapshot(name, snapshot):
    '''CreateVNodeSnapshot(name, snapshot)
    - Def -
        Creates a snapshot for a VNode.
    - Args -
        . name [str]     --> name of the desired virtual node
        . snapshot [str] --> snapshot tag name
    '''
    try:
        node = VNode.set[name]
        printc('Creating snapshot ' + str(snapshot) + ' for VNode ' + str(name), 'cyan')
        node.send("savevm " + str(snapshot))
        node.send("info snapshots")
    except KeyError:
        raise NemuError('Cannot find the Virtual Node ' + str(i))

def LoadVNodeSnapshot(name, snapshot):
    '''LoadVNodeSnapshot(name, snapshot)
    - Def -
        Loads a snapshot for a VNode.
    - Args -
        . name [str]     --> name of the desired virtual node
        . snapshot [str] --> snapshot tag name
    '''
    try:
        node = VNode.set[name]
        printc('Loading snapshot ' + str(snapshot) + ' for VNode ' + str(name), 'cyan')
        node.send("loadvm " + str(snapshot))
    except KeyError:
        raise NemuError('Cannot find the Virtual Node ' + str(i))
