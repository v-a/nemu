# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

'''This module manages virtual links VQHub'''

from nemu.msg import (printc, printok, printstr)
from nemu.vlink import VLink
from nemu.error import NemuError
from nemu.var import NemuVar

class VQHub(VLink):
    '''VQHub(name, niface)
    - Def -
        A VQHub represents a virtual Ethernet QEMU hub.
    - Args -
        . name [str] --> VLink name
        . niface [str] --> number of virtual interfaces
    '''
    set = dict()
    def __init__(self, name, niface):
        printc('Setting up VQHub ' + str(name), 'blue')
        VLink.__init__(self, name, "qhub", niface)
        VQHub.set[name] = self
        self.qemu = NemuVar.qemu
        printok()

    def delete(self):
        printc('Deleting the VQHub ' + str(self.name) , 'blue')
        VLink.delete(self)
        del VQHub.set[self.name]
        printok()

    def addlink(self, id):
        return

    def dellink(self, id):
        return

    def confiface(self, id, direction, state, vlan, bandwidth, delay, jitter, ber, qsize):
        raise NemuError("Interface properties are not available for VQHub")

    def unconfiface(self, id, direction, state, vlan, bandwidth, delay, jitter, ber, qsize):
        raise NemuError("Interface properties are not available for VQHub")

    def unixiface(self, id, path, type):
        raise NemuError('UNIX is not available for VQHub')

    def dumplink(self, id):
        iface = self.findiface(id)
        if self.running() and iface.dump:
            com = "object_add filter-dump,id=dump." + str(iface.id) + ",netdev=endpoint." + str(iface.id) + ",file=" + str(iface.dumpfile)
            self.send(com)

    def undumplink(self, id):
        iface = self.findiface(id)
        if self.running():
            com = "netdev_del dump." + str(iface.id)
            self.send(com)

    def setlink(self, id):
        iface = self.findiface(id)
        if self.running() and iface.activ:
            self.unsetlink(id)
            com = "netdev_add "
            if iface.proto == 'mcast':
                com += "socket,mcast=" + str(iface.addr) + ":" + str(iface.port)
                if iface.laddr:
                    com += ",localaddr="  + str(iface.laddr)
            elif iface.proto == 'udp':
                if iface.type == 'srv':
                    com += "socket,udp=" + str(iface.laddr) + ":" + str(iface.lport) + ",localaddr=" + str(iface.addr) + ':' +  str(iface.port)
                elif iface.type == 'cli':
                    com += "socket,udp=" + str(iface.addr) + ":" + str(iface.port) + ",localaddr=" + str(iface.laddr) + ':' +  str(iface.lport)
            elif iface.proto == 'tcp':
                proto = None
                if iface.type == 'srv':
                    proto = 'listen'
                elif iface.type == 'cli':
                    proto = 'connect'
                com += "socket," + str(proto) + "=" + str(iface.addr) + ":" + str(iface.port)
            elif iface.proto == 'vde':
                com = "vde,sock=" + str(iface.path)
            elif iface.proto == 'tap':
                com = "tap,ifname=" + str(iface.ifname) + ",script=" + str(NemuVar.tapup) + ",downscript=" + str(NemuVar.tapdown)
            com += ",id=socket." + str(iface.id)
            self.send(com)
            com = "netdev_add hubport,id=endpoint." + str(iface.id) + ",netdev=socket." + str(iface.id) + ",hubid=0"
            self.send(com)
            self.dumplink(id)

    def unsetlink(self, id):
        iface = self.findiface(id)
        if self.running():
            self.undumplink(id)
            self.send("netdev_del socket." + str(iface.id))
            self.send("netdev_del endpoint." + str(iface.id))

    def qstr(self):
        return str(self.qemu) + ' -name ' + str(self.name)  + ' -monitor stdio -m 2 -display none -nic none'

    def __str__(self):
        com = self.__class__.__name__ + '('
        com += 'name=' + printstr(self.name) + ',niface=' + printstr(len(self.ifaces)) + ')'
        com += VLink.__str__(self)
        return com

    def __repr__(self):
        return str(self)
