# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

'''This module manages NEmu graph visualization'''

from nemu.error import NemuError
from nemu.vnode import VNode
from nemu.vlink import VLink
from nemu.nbd import RemoteNBD
from nemu.mobile import MobNemu
from nemu.remote import RemoteNemu
from nemu.var import NemuVar
from nemu.msg import (printc, printok, printfd)
from nemu.proc import dirproc

import os.path

def get_log_file(name, direction):
    '''Internal function to retreive a log file'''
    logfile = None
    if not name:
        logfile = NemuVar.logf
    elif name in VNode.set:
        node = VNode.set[name]
        if direction == 'in':
            logfile = node.infile
        elif direction == 'out':
            logfile = node.logfile
        else:
            raise NemuError(direction + ' is not a valid log file direction')
    elif name in VLink.set:
        link = VLink.set[name]
        if direction == 'in':
            logfile = link.infile
        elif direction == 'out':
            logfile = link.logfile
        else:
            raise NemuError(direction + ' is not a valid log file direction')
    elif name in MobNemu.set:
        mob = MobNemu.set[name]
        if direction == 'in':
            logfile = mob.infile
        elif direction == 'out':
            logfile = mob.logfile
        else:
            raise NemuError(direction + ' is not a valid log file direction')
    elif name in RemoteNBD.set:
        remote = RemoteNBD.set[name]
        if direction == 'in':
            logfile = None
        elif direction == 'out':
            logfile = remote.logfile
        else:
            raise NemuError(direction + ' is not a valid log file direction')
    elif name in RemoteNemu.set:
        remote = RemoteNemu.set[name]
        if direction == 'in':
            logfile = None
        elif direction == 'out':
            logfile = remote.logfile
        else:
            raise NemuError(direction + ' is not a valid log file direction')
    else:
        raise NemuError('cannot find any element called ' + str(name))
    if logfile and os.path.isfile(logfile) and os.access(logfile, os.R_OK):
        return logfile
    else:
        raise NemuError('no logfile ' + str(direction) + ' found or readable for ' + str(name))

def LogNemu(name=None, nline=0, direction='out', dest=None):
    '''LogNemu(name, nline=0, direction, dest)
    - Def -
        Exports internal logs into a text file or on stdout.
        The full or partial process log file is get from of a specified element.
    - Args -
        . name [str] --> name of the target element (default: session logs)
        . nline[int] --> 0 for full content, positive value for head and negative value for tail (Default: 0)
        . direction [str] --> "in" or "out" (Default: out)
        . dest [str] --> output filename (Default: None)
    '''
    if not isinstance(nline, int):
        raise NemuError("number of lines to print has to an integer value")
    logfile = get_log_file(name, direction);
    if logfile:
        fd = None
        out = None
        if dest:
            printc('Exporting ' + str(name) + ' log to ' + str(dest), 'blue')
            fd = open(dest, 'w')
        if nline == 0:
            dirproc("cat " + str(logfile), fd)
        elif nline > 0:
            dirproc("head -" + str(abs(nline)) + " " + str(logfile), fd)
        else:
            dirproc("tail -" + str(abs(nline)) + " " + str(logfile), fd)
        if dest:
            fd.close()
            printok()
