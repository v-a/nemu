# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

'''This module contains functions for VContainer'''

import os
import shutil

from nemu.name import NemuNameSpace
from nemu.var import NemuVar
from nemu.error import NemuError
from nemu.msg import (printc, printok, printstr)
from nemu.path import mpath
from nemu.vfs import VFs
from nemu.vnode import VNode
from nemu.container import (Container, ContainerFS, ContainerNetwork)

class VContainerConf():
    '''VContainerConf(name, run, before, after, kmap, ntp, swap, **opts)
    - EXPERIMENTAL FEATURE -
    - Def -
        A VContainerConf is a common configuration for several VContainers.
    - Args -
        . name [str] --> VContainerConf name
        . run [Container] --> Container instance (Default: None)
        . before [list of Container] --> Container instances launched in background before the main run container (Default: empty)
        . after [list of Container] --> Container instances launched in after the main run container (Default: empty)
        . kmap [str] --> keyboard layout identifier (Default: none)
        . ntp [str] --> NTP server (Default: none)
        . swap [str] --> swap memory value (Default: none)
        . opts [dict] --> other qemu options. a=1, b_c=None, d=["y","z"] -> qemu -a 1 -b-c -d y -d z (Default: empty)
    '''
    set = dict()
    def __init__(self, name, run=None, before=list(), after=list(), kmap=None, ntp=None, swap=None, **opts):
        printc('Setting up VContainerConf ' + str(name), 'blue')
        if name in VContainerConf.set:
            raise NemuError('VContainerConf ' + str(name) + ' already exists')
        if name in NemuNameSpace.set:
            raise NemuError(str(name) + ' already exists in the current namespace')
        self.name = str(name)
        self.run = run
        self.kmap = kmap
        self.ntp = ntp
        self.swap = swap
        self.before = list()
        self.after = list()
        self.opts = dict()
        if self.run and not isinstance(self.run, Container):
            raise NemuError(str(self.run) + ' is not a Container')
        for i in before:
            if not isinstance(i, Container):
                raise NemuError(str(i) + ' is not a Container')
            self.before.append(i)
        for i in after:
            if not isinstance(i, Container):
                raise NemuError(str(i) + ' is not a Container')
            self.after.append(i)
        for i in list(opts.keys()):
            self.opts[i.replace('_', '-')] = opts[i]
        VContainerConf.set[name] = self
        NemuNameSpace.set[name] = self
        printok()

    def delete(self):
        '''Deletes the vcontainer configuration'''
        printc('Deleting the VContainerConf ' + str(self.name) , 'blue')
        del VContainerConf.set[self.name]
        del NemuNameSpace.set[self.name]
        printok()

class VContainer(VNode):
    '''VContainer(name, conf, hds, nics, networks, fs, volumes, cache, run, before, after, kmap, ntp, swap, **opts)
    - EXPERIMENTAL FEATURE -
    - Def -
        A VContainer is a Podman container embed into a QEMU virtual node.
    - Args -
        . name [str] --> VContainer name
        . conf [str] --> VContainerConf name (Default: None)
        . hds [list of VFs] --> hard drives list (Default: empty)
        . nics [list of VNic] --> network interface cards list (Default: empty)
        . networks [list of ContainerNetwork] --> Container network configurations list (Default: empty)
        . fs [list of ContainerFS] --> Container filesystem configurations list (Default: empty)
        . volumes [list of str] --> volumes list (Default: empty)
        . cache [str] --> cache device (Default: None)
        . run [Container] --> Container instance (Default: None)
        . before [list of Container] --> Container instances launched in background before the main run container (Default: empty)
        . after [list of Container] --> Container instances launched in after the main run container (Default: empty)
        . kmap [str] --> keyboard layout identifier (Default: none)
        . ntp [str] --> NTP server (Default: none)
        . swap [str] --> swap memory value (Default: None)
        . opts [dict] --> other qemu options. a=1, b_c=None, d=["y","z"] -> qemu -a 1 -b-c -d y -d z (Default: empty)
    '''
    set = dict()
    def __init__(self, name, conf=None, hds=list(), nics=list(), networks=list(), fs=list(), volumes=list(), cache=None, run=None, before=list(), after=list(), kmap=None, ntp=None, swap=None, **opts):
        if name in VContainer.set:
            raise NemuError('VContainer ' + str(name) + ' already exists')
        if name in NemuNameSpace.set:
            raise NemuError(str(name) + ' already exists in the current namespace')
        self.name = str(name)
        self.run = None
        self.kmap = None
        self.ntp = None
        self.swap = None
        self.before = list()
        self.after = list()
        self.cache = None
        self.networks = list()
        self.fs = list()
        self.volumes = list()
        self.opts = dict()
        if not conf == None:
            printc('Setting up VContainer ' + str(name) + ' with the VContainerConf ' + str(conf), 'blue')
            try:
                self.conf = VContainerConf.set[conf]
            except KeyError:
                raise NemuError('Cannot find the VContainerConf ' + str(conf))
            self.run = self.conf.run
            self.kmap = self.conf.kmap
            self.ntp = self.conf.ntp
            self.swap = self.conf.swap
            self.after.extend(self.conf.after)
            self.before.extend(self.conf.before)
            self.opts.update(self.conf.opts)
        else:
            printc('Setting up VContainer ' + str(name), 'blue')
        if run:
            self.run = run
        if not isinstance(self.run, Container):
            raise NemuError(str(self.run) + ' is not a Container')
        if swap:
            self.swap = swap
        if kmap:
            self.kmap = kmap
        if ntp:
            self.ntp = ntp
        self.cache = cache
        for i in before:
            if not isinstance(i, Container):
                raise NemuError(str(i) + ' is not a Container')
            self.before.append(i)
        for i in after:
            if not isinstance(i, Container):
                raise NemuError(str(i) + ' is not a Container')
            self.after.append(i)
        for i in networks:
            if not isinstance(i, ContainerNetwork):
                raise NemuError(str(i) + ' is not a ContainerNetwork')
            self.networks.append(i)
        for i in fs:
            if not isinstance(i, ContainerFS):
                raise NemuError(str(i) + ' is not a ContainerFS')
            self.fs.append(i)
        for i in volumes:
            if not isinstance(i, str):
                raise NemuError(str(i) + ' is not a string')
            self.volumes.append(i)
        self.opts.update(opts)
        self.kernel = mpath(NemuVar.rcd, NemuVar.rcdb2c + '-' + NemuVar.rcdb2ckernel)
        self.initrd = mpath(NemuVar.rcd, NemuVar.rcdb2c + '-' + NemuVar.rcdb2cinitrd)
        VNode.__init__(self, self.name, hds=hds, nics=nics, qemu=NemuVar.qemu, **self.opts)
        VContainer.set[name] = self
        printok()

    def genops(self):
        '''Sets all features'''
        printc('Setting up configuration of VContainer ' + str(self.name), 'blue')
        boot = mpath(NemuVar.nemurcd, NemuVar.rcdboot)
        kernel = mpath(NemuVar.nemurcd, NemuVar.rcdb2c, NemuVar.rcdb2ckernel)
        initrd = mpath(NemuVar.nemurcd, NemuVar.rcdb2c, NemuVar.rcdb2cinitrd)
        if not os.path.isfile(self.kernel):
            shutil.copyfile(kernel, self.kernel)
        if not os.path.isfile(self.initrd):
            shutil.copyfile(initrd, self.initrd)
        printok()

    def start(self):
        self.genops()
        VNode.start(self)

    def stop(self):
        if self.running():
            VNode.stop(self)

    def __str__(self):
        com = 'VContainer('
        com += 'name=' + printstr(self.name)
        com += ',' + self.estr_hds()
        com += ',' + self.estr_net()
        com += ',networks=['
        for i in self.networks:
            com += str(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ',fs=['
        for i in self.fs:
            com += str(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ',volumes=['
        for i in self.volumes:
            com += printstr(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ',cache=' + printstr(self.cache)
        com += ',run=' + printstr(self.run)
        com += ',before=['
        for i in self.before:
            com += str(i) + ','
        com = com.rstrip(',')
        com += ']'
        com += ',after=['
        for i in self.after:
            com += str(i) + ','
        com += ']'
        com = com.rstrip(',')
        com += ',kmap=' + printstr(self.kmap)
        com += ',ntp=' + printstr(self.ntp)
        com += ',swap=' + printstr(self.swap)
        com += ',**' + printstr(self.opts)
        com += ')'
        return com

    def qstr(self):
        ret = VNode.qstr(self)
        ret += '-kernel ' + str(self.kernel) + ' '
        ret += '-initrd ' + str(self.initrd) + ' '
        ret += '-append \'b2c.hostname=' + str(self.name)
        if self.kmap:
            ret += ' b2c.keymap=' + str(self.kmap)
        if self.ntp:
            ret += ' b2c.ntp=' + str(self.ntp)
        if self.swap:
            ret += ' b2c.swap=' + str(self.swap)
        for i in self.fs:
            ret += ' ' + i.qstr()
        for i in self.volumes:
            ret += ' b2c.volume=' + str(i)
        if self.cache:
            ret += ' b2c.cache_device=' + str(self.cache)
        if self.networks:
            for i in self.networks:
                ret += ' ' + i.qstr()
        else:
            ret += ' ip=none'
        ret += ' b2c.run="' + self.run.qstr() + '"'
        for i in self.before:
            ret += ' b2c.run_service="' + i.qstr() + '"'
        for i in self.after:
            ret += ' b2c.run_post="' + i.qstr() + '"'
        ret += '\''
        return ret

    def __repr__(self):
        return str(self)

    def delete(self):
        printc('Deleting the VContainer ' + str(self.name) , 'blue')
        self.stop()
        del VContainer.set[self.name]
        VNode.delete(self)
        printok()
