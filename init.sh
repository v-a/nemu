#!/bin/bash

# ---------------------------------
# -- NEmu : The Network Emulator --
# ---------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0");
SCRIPT_PATH=$(dirname "$SCRIPT");
LOG_PATH="$SCRIPT_PATH/init.log";

BUILD_STATIC_BOOST=0;
BUILD_STATIC_OPENSSL=0;
BUILD_STATIC_VDE=0;

function __get_time(){
  _GLOBAL_TIME_NOW="$(date '+%Y/%m/%d %H:%M:%S')";
}

function __do_info(){
  __get_time;
  echo -e "[Info]    $1";
  echo -e "[$_GLOBAL_TIME_NOW - Info]    $1" >> $LOG_PATH;
}

function __do_warning(){
  __get_time;
  echo -e "[Warning] $1";
  echo -e "[$_GLOBAL_TIME_NOW - Warning]  $1" >> $LOG_PATH;
}

function __do_error(){
  __get_time;
  echo -e "[Error]   $1";
  echo -e "[$_GLOBAL_TIME_NOW - Error]    $1" >> $LOG_PATH;
  __do_info "log available at $LOG_PATH";
  exit 1;
}

function __test_exit(){
  if [ $? -eq 0 ]
  then
    __do_error "$1";
  fi
}

function __fail_exit(){
  if ! [ $? -eq 0 ]
  then
    __do_error "$1";
  fi
}

function __check_build_dependency(){
  TEST_EXEC=$1;
  __do_info "checking for build dependency $TEST_EXEC";
  which $TEST_EXEC &>> $LOG_PATH;
  if ! [ $? -eq 0 ]
  then
    alias $TEST_EXEC &>> $LOG_PATH;
    __fail_exit "cannot find dependency $TEST_EXEC";
  fi
}

function __check_run_dependency(){
  TEST_EXEC=$1;
  __do_info "checking for run dependency $TEST_EXEC";
  which $TEST_EXEC &>> $LOG_PATH;
  if ! [ $? -eq 0 ]
  then
    alias $TEST_EXEC &>> $LOG_PATH;
    if ! [ $? -eq 0 ]
    then
      __do_warning "cannot find dependency $TEST_EXEC";
      return 1;
    fi
  fi
  return 0;
}

function check_dependencies(){
  __check_build_dependency cmake;
  __check_build_dependency make;
  __check_build_dependency g++;
  __check_run_dependency python3;
  __check_run_dependency qemu-system-x86_64;
  __check_run_dependency qemu-img;
  __check_run_dependency mksquashfs;
  __check_run_dependency ssh;
  __check_run_dependency sshfs;
  __check_run_dependency dot;
  __check_run_dependency vde_switch;
}

function build_vnd(){
  __do_info "building nemu.vnd";
  VND_HOME="$SCRIPT_PATH/rcd/vnd";
  VND_BUILD="$SCRIPT_PATH/rcd/vnd/build";
  if ! [ -d $VND_HOME ]
  then
    __do_error "cannot find vnd directory $VND_HOME";
  fi
  rm -rf $VND_BUILD;
  local BUILD_OPTIONS="";
  [ $BUILD_STATIC_BOOST -gt 0 ] && BUILD_OPTIONS+="-DBUILD_STATIC_BOOST=ON ";
  [ $BUILD_STATIC_OPENSSL -gt 0 ] && BUILD_OPTIONS+="-DBUILD_STATIC_OPENSSL=ON ";
  [ $BUILD_STATIC_VDE -gt 0 ] && BUILD_OPTIONS+="-DBUILD_STATIC_VDE=ON ";
  (mkdir -p $VND_BUILD && cd $VND_BUILD && cmake $BUILD_OPTIONS .. && make -j && make install && cd - && rm -rf $VND_BUILD) &>> $LOG_PATH;
  __fail_exit "cannot build nemu.vnd";
}

function build_nemo(){
  __do_info "building nemu.nemo";
  NEMO_HOME="$SCRIPT_PATH/rcd/nemo";
  NEMO_BUILD="$SCRIPT_PATH/rcd/nemo/build";
  if ! [ -d $NEMO_HOME ]
  then
    __do_error "cannot find vnd directory $NEMO_HOME";
  fi
  rm -rf $NEMO_BUILD;
  local BUILD_OPTIONS="";
  [ $BUILD_STATIC_BOOST -gt 0 ] && BUILD_OPTIONS+="-DBUILD_STATIC_BOOST=ON ";
  (mkdir -p $NEMO_BUILD && cd $NEMO_BUILD && cmake $BUILD_OPTIONS .. && make -j && make install && cd - && rm -rf $NEMO_BUILD) &>> $LOG_PATH;
  __fail_exit "cannot build nemu.nemo";
}

HELP="Usage: $SCRIPT [-h] [-v] [--static-boost] [--static-openssl] [--static-vde] [--static]"

function __do_help(){
  echo "$HELP";
  echo -e "\t -h|--help \t\t help... what else ?";
  echo -e "\t -v|--version \t\t version";
  echo -e "\t --static \t\t enable static build for Boost, OpenSSL and VDE";
  echo -e "\t --static-boost \t enable static build for Boost";
  echo -e "\t --static-openssl \t enable static build for OpenSSL";
  echo -e "\t --static-vde \t\t enable static build for VDE";
  exit 0;
}

function __do_usage(){
  echo "$HELP";
  exit 1;
}

function __do_version(){
  $SCRIPT_PATH/nemu.py --version
  exit 0;
}


OPTS=$(getopt -n $SCRIPT -o hv -l help,version,static,static-boost,static-openssl,static-vde -- "$@")
ARGS="$*";
eval set -- "$OPTS";
while true ; do
  case "$1" in
    -h|--help) __do_help ; shift;;
    -v|--version) __do_version ; shift;;
    --static) BUILD_STATIC_BOOST=1; BUILD_STATIC_OPENSSL=1; BUILD_STATIC_VDE=1; shift;;
    --static-boost) BUILD_STATIC_BOOST=1; shift;;
    --static-openssl) BUILD_STATIC_OPENSSL=1; shift;;
    --static-vde) BUILD_STATIC_VDE=1; shift;;
    --) shift; break;;
    *) __do_usage;;
  esac
done

echo -n > $LOG_PATH;
check_dependencies;
build_vnd;
build_nemo;
__do_info "log available at $LOG_PATH";
