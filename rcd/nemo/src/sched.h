
// sched (schedulers)

// nemo (network mobilizer)
// Copyright (C) 2012-2019 Damien Magoni

/*
    This file is part of nemo.

    nemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    nemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with nemo.  If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined(SCHED_H)
#define SCHED_H

//#include <boost/chrono/chrono.hpp>
#include <boost/thread/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>
#include <boost/regex.hpp>
#include <boost/asio.hpp>

#include "mt19937.h"
#include "prost.h"
#include "medeb.h"
//#include "mesap.h"
#include "util.h"
//#include "nemo.h"

typedef boost::chrono::duration<double> fpsec;
typedef boost::chrono::high_resolution_clock::time_point time_type;
typedef boost::asio::basic_waitable_timer<boost::chrono::high_resolution_clock> timer_type;

namespace nemo
{
	class action_type;
	class actor
	{
	public:
		virtual void execute(action_type * a);
		virtual void execute(const boost::system::error_code & ec, action_type * a);
		virtual ~actor();
	};

	class acted
	{
	public:
		virtual ~acted();
	};

	typedef int8_t action_status_type;
	const action_status_type queued = 1;
	const action_status_type started = 2;
	const action_status_type finished = 3;
	const action_status_type cancelled = 4;

	typedef dword action_id_type;

	class action_type
	{
	public:
		action_status_type status;
		action_id_type id;

		// partially used
		time_type call_time; //start_time, stop_time; // when the handler is called
		// for use by RT and nRT
		nsec delta_call_time; // time expressed in duration from a start time always set at 0

		nsec duration;
		fpsec duration_in_s;

		std::string description; // what does the action
		std::string handler; // what to do at the call time (end) of the action
		actor * doer; // the one who does the action (subject)
		acted * doed; // the one upon whom the action is done (object)
	};

	class network_mobilizer;

	class scheduler_type
	{
		public:
			bool running;
			time_type start_time, current_time;
			nsec scenario_duration;
			fpsec scenario_duration_in_s;
			action_id_type action_id_counter;
			std::multimap<nsec, action_type *> actions;
			std::multimap<nsec, action_type *>::iterator ait;

			//std::multimap<time_type, action_type *> actions; // time_type should be replaced by nsec in RT...
			//std::multimap<time_type, action_type *>::iterator ait;

			//mesap::messenger * error, * warning, * output;
			network_mobilizer * nemo_ptr;

			void destroy_actions();
	};

	class nrt_scheduler_type : public scheduler_type
	{
	public:
		nsec time_step_in_ns; // granularity, sampling interval, etc

		nrt_scheduler_type();
		~nrt_scheduler_type();
		void run();
		void reset();
		action_id_type schedule(action_type * a); // sched later event
		bool cancel(action_id_type id);	// cancel event
	};

	class rt_scheduler_type : public scheduler_type
	{
	public:
		boost::asio::io_service * io_serv;
		boost::asio::io_service::strand * stran;
		timer_type * timer;
		bool timer_running;
		//time_type epoch_time, last_action_time, action_call_time;
		nsec offset_duration;

		rt_scheduler_type();
		~rt_scheduler_type();
		void start();
		void pause();
		void resume();
		void stop();
		void status();
		action_id_type schedule(action_type * a); // create action list
		void wait_till_next(); // sched next event
		//bool cancel(action_id_type id);	// cancel event
	};
}

#endif
