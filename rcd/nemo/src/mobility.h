
// nemo (network mobilizer)
// Copyright (C) 2012-2019 Damien Magoni

/*
	This file is part of nemo.

	nemo is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of
	the License, or (at your option) any later version.

	nemo is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with nemo.  If not, see <http://www.gnu.org/licenses/>.
*/

//#ifndef MOBILITY_H
#if !defined(MOBILITY_H)
#define MOBILITY_H

#include "sched.h"

namespace nemo
{
	typedef double unit;

	class position_type
	{
	public:
		unit x, y, z;
	};
	class velocity_type
	{
	public:
		unit x, y, z;
	};
	class acceleration_type
	{
	public:
		unit x, y, z;
	};
	class jerk_type
	{
	public:
		unit x, y, z;
	};

	class voxel_type
	{
	public:
		unit x, y, z;
		double attenuation;
	};

/*
	file formats for maps

	# setting = flat
	# name x_size y_size z_size space_step
	# mapflat xs ys zs ss

	# setting = surface
	# name x_size y_size z_size space_step
	# mapsurf xs ys zs ss
	#
	# x1 y1 z1 a1
	# x2 y2 z2 a2
	# etc

	# setting = surface
	# name x_size y_size z_size space_step
	# mapspace xs ys zs ss
	# a1(0,0,0)
	# a2(0,0,1)
	# etc
*/

	typedef int8_t map_setting_type;
	const map_setting_type map_flat = 1; // z = 0, attn = 0
	const map_setting_type map_surface = 2; // only one point z1 in z for each (x, y), attn = a(x, y) < 0 for z <= z1
	const map_setting_type map_space = 3; // one or more points zi in z for each (x, y), attn = a(x, y, z) <= 0

	class network_mobilizer;

	class map_type
	{
	public:
		network_mobilizer * nemo_ptr;
		std::string map_name;
		map_setting_type map_setting;
		unit x_size, y_size, z_size;
		double space_step_in_m; // 1 = 1 meter
		typedef std::vector<unit> line;
		typedef std::vector<line> plane;
		std::vector<plane> space;

		void generate(std::vector<std::string> & v);
		void print(std::ostream & ost);
		void load(std::vector<std::string> & v);
		void save(std::vector<std::string> & v);
	};

	typedef int8_t wic_type;
	const wic_type wic_cisco_aironet = 1;
	const wic_type wic_3com_xjack = 2;
	const wic_type wic_lucent_wavelan = 3;

	const double default_tx_power = 20.0; // dBm
	const double default_margin = 10.0; // dB
	const double default_carrier_freq = 2.4897E9; // Hz

	class mobile_device_type
	{
	public:
		dword id;
		wic_type wic;
		double tx_power; // in dBm
		double margin; // in dBm
		double carrier_freq; // in Hz

		position_type position;
		velocity_type velocity;
		acceleration_type acceleration;
		//jerk_type jerk;

		nsec delta_time; // relative to start time
		nsec previous_delta_time;
		//time_type last_move_time; // = start_time + delta_time, for ns2

		// for ns2 import
		nsec ns_curr_time, ns_dest_time;
		position_type ns_curr_position, ns_dest_position;
		velocity_type ns_curr_velocity;
		acceleration_type ns_curr_acceleration;

		void set(std::string par, std::string val);
	};

	class mobility_event_type : public acted
	{
	public:
		nsec delta_time; // relative to start time

		//time_type time; // = start_time + delta_time
		//nsec duration; // or use another event to stop/modify current event? yes

		dword device_id;
		velocity_type velocity;
		acceleration_type acceleration;
		//jerk_type jerk;
	};

	class connectivity_event_type;

	class mobility_scenario_type : public actor
	{
	public:
		network_mobilizer * nemo_ptr;

		std::string name;
		nrt_scheduler_type nrt_scheduler;

		std::multimap<nsec, mobility_event_type *> mobility_events;
		std::multimap<nsec, mobility_event_type *>::iterator itme;

		std::map<dword, mobile_device_type *>::iterator mdit, mdit2;

		typedef std::map<dword, connectivity_event_type *> reachable_mobile_type; // list storing all devices able to receive from (connected to) a given device with latest ce
		reachable_mobile_type::iterator rmtit;
		std::map<dword, reachable_mobile_type> reachable_mobiles; // stores the connectivity list of all devices
		std::map<dword, reachable_mobile_type>::iterator rmit;

		mobility_scenario_type();
		~mobility_scenario_type();
		void generate(std::vector<std::string> & v);
		void print(std::ostream & ost);
		void load(std::vector<std::string> & v);
		void load_mbl(std::string & fn);
		void load_ns2(std::string & fn);
		void save(std::vector<std::string> & v);
		void save_mbl(std::string & fn);
		void save_ns2(std::string & fn);
		void calculate_velocity(mobile_device_type * m, double final_x, double final_y, double modv, nsec st);
		void update_mobile_device(mobile_device_type * md, nsec delta_time);
		void node(std::vector<std::string> & v);
		dword get_node_id(std::string t);
		double distance(position_type p1, position_type p2);
		double bitrate(double rx_level_in_dBm, wic_type wt);
		connectivity_event_type * are_connected(dword md1, dword md2);

		void process(std::vector<std::string> & v); // create connectivity events through nrts
		void execute(action_type * a);

	};
}

#endif // MOBILITY_H

/*
#	Steady-state random_l Waypoint Model
#	numNodes   =     50
#	maxX       =   1000.00
#	maxY       =   1000.00
#	endTime    =   5000.00
#	speedMean  =     10.0000
#	speedDelta =      9.0000
#	pauseMean  =     20.00
#	pauseDelta =     20.00
#	output     =      N

# output format is NS2
#	Initial positions:
$node_(0) set X_ 258.270521359214
$node_(0) set Y_ 102.679585225733
$node_(0) set Z_ 0.000000000000
$ns_ at 0.000000000000 "$node_(0) setdest 266.306154088260 50.521009625178 0.000000000000"
$node_(1) set X_ 584.254405823678
$node_(1) set Y_ 800.471645783187
$node_(1) set Z_ 0.000000000000
$ns_ at 0.000000000000 "$node_(1) setdest 530.295056072201 957.574445734534 7.159503693794"
$node_(2) set X_ 555.169843720718
$node_(2) set Y_ 749.766645413628
$node_(2) set Z_ 0.000000000000
$ns_ at 0.000000000000 "$node_(2) setdest 523.781282139840 791.876172084304 1.206961050981"
...

#	Movements:
# start_time node_id setdest final_x final_y speed
$ns_ at 2.285848421030 "$node_(29) setdest 305.833337039609 718.973127994208 0.000000000000"
$ns_ at 2.588910350452 "$node_(8) setdest 887.838492583408 996.248911133152 0.000000000000"
$ns_ at 5.613500611207 "$node_(45) setdest 607.727486457549 671.039295229613 0.000000000000"
$ns_ at 6.143589350314 "$node_(29) setdest 313.968211558633 20.583679909158 13.094838077247"
$ns_ at 6.237715639613 "$node_(13) setdest 689.162453026121 838.848721626610 0.000000000000"
$ns_ at 6.797565901677 "$node_(11) setdest 604.574759772315 255.062498271029 0.000000000000"
$ns_ at 7.047415435719 "$node_(47) setdest 311.538312263572 118.912923205138 14.663742882043"
$ns_ at 7.346616714985 "$node_(38) setdest 810.975851403072 588.811216684436 0.000000000000"
...

void DmpAgent::set_range()
{
	double power = transmission_power - margin_ - rx_thr_;
	// power to distance formulae
	transmission_range = pow(10, 0.05 * (power - 20 * log10(4 * acos(-1) / 300000000) - 20 * log10(2.4897 * 1000000000)));
	// TO COMMENT LATER
	//transmission_range = range_max_;
}
void DmpAgent::set_power()
{
	energy_used = energy_used + pow(10, transmission_power/10) * (Scheduler::instance().clock() - transmission_power_change_time) / 1000;
	transmission_power_change_time = Scheduler::instance().clock();
	// distance to power formulae
	double attenuation = 20 * log10(4 * acos(-1) / 300000000) + 20 * log10(2.4897*1000000000) + 20 * log10(transmission_range);
	transmission_power = attenuation + margin_ + rx_thr_;
	if ( transmission_power > tx_max_ )
			transmission_power = tx_max_;
}
*/
