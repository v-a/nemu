
// nemo (network mobilizer)
// Copyright (C) 2012-2019 Damien Magoni

/*
    This file is part of nemo.

    nemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    nemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with nemo.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nemo.h"

namespace nemo
{
	network_mobilizer::network_mobilizer(std::map<std::string, std::string> arg) :
		error(arg["-d"] + "nemo_"+arg["-p"]+"_error.log", mesap::console|mesap::logging, false),
		warning(arg["-d"] + "nemo_"+arg["-p"]+"_warning.log", mesap::console|mesap::logging, false),
		output(arg["-d"] + "nemo_"+arg["-p"]+"_output.log", mesap::console, false)
	{
		args = arg;
		name = "nemo-" + version;
		map_loaded = false;
		mbl_loaded = false;
		cnn_loaded = false;
		debug = false;
		trace = false;
		exit_now = false;

		port_seed = static_cast<word>(atol(arg["-p"].c_str()));
		tcp_base_source_port = port_seed * 100;
		udp_base_source_port = port_seed * 100;
		tcp_resolver = 0;
		udp_resolver = 0;
		stran = 0;
		map.nemo_ptr = this;
		mobility_scenario.nemo_ptr = this;
		mobility_scenario.nrt_scheduler.nemo_ptr = this;
/*		mobility_scenario.nrt_scheduler.error = &error;
		mobility_scenario.nrt_scheduler.warning = &warning;
		mobility_scenario.nrt_scheduler.output = &output;
*/		mobility_scenario.nrt_scheduler.scenario_duration = nsec(0);
		connectivity_scenario.nemo_ptr = this;
		connectivity_scenario.rt_scheduler.nemo_ptr = this;
/*		connectivity_scenario.rt_scheduler.error = &error;
		connectivity_scenario.rt_scheduler.warning = &warning;
		connectivity_scenario.rt_scheduler.output = &output;
*/		connectivity_scenario.rt_scheduler.scenario_duration = nsec(0);
	}

	network_mobilizer::~network_mobilizer()
	{
		itmd = mobile_devices.begin();
		while ( itmd != mobile_devices.end() )
		{
			if ( itmd->second )
			{
				if ( medeb::active ) medeb::delm(itmd->second);
				delete itmd->second;
			}
			itmd++;
		}
		mobile_devices.clear();

		itep = end_points.begin();
		while ( itep != end_points.end() )
		{
			if ( itep->second )
			{
				if ( medeb::active ) medeb::delm(itep->second);
				delete itep->second;
			}
			itep++;
		}
		end_points.clear();

		if ( tcp_resolver )
		{
			delete tcp_resolver;
		}
		if ( udp_resolver )
		{
			delete udp_resolver;
		}
		// useless
		if ( stran )
			delete stran;
	}

	void network_mobilizer::create_frame(frame * f)
	{
	}

	void network_mobilizer::destroy_frame(frame * f)
	{
		f->ref_count--;
		if ( f->ref_count < 1 )
		{
			if ( medeb::active ) medeb::delm(f);
			delete f;
			f = 0;
		}
	}

	void network_mobilizer::load(std::string s)
	{
		if ( s.empty() )
			return;
		std::ifstream f(s.c_str());
		if ( !f )
		{
			error << "failed to open configuration file: " << s << "\n";
			return;
		}
		while ( !f.eof() )
		{
			std::string l;
			std::getline(f, l); // add error mgmt
			if ( !l.find("#") || !l.size() )//== std::string::npos ) // # must be at beginning of line!
				continue;
			//if ( !l.find("exit") ) // "exit" must be at beginning of line!
			//	return;
			// exit is a real order that should be sent to io_serv
			if ( debug )
				output << "> posting for execution: " << l << "...\n";
			stran->post(boost::bind(&network_mobilizer::command, this, l)); // shall we post the command? is the post order preserved?
		}
		f.close();
	}

	void network_mobilizer::prompt()
	{
		output << name << " % ";
	}

	void network_mobilizer::process()
	{
		try
		{
			tcp_resolver = new boost::asio::ip::tcp::resolver(*io_serv);
			udp_resolver = new boost::asio::ip::udp::resolver(*io_serv);
		}
		catch (std::exception & e)
		{
			warning << "> exception in process : " << e.what() << "\n% ";
		}

		std::unique_ptr<boost::asio::io_service::work> work(new boost::asio::io_service::work(*io_serv)); // shall work be a local var?
		// launch a new thread that executes io_service::run()
		boost::thread thread_for_io_serv_run(boost::bind(&boost::asio::io_service::run, io_serv)); // shall it be a local var?

		if ( args.find("-f") != args.end() && args["-f"] != "" )
		{
			// load only calls stran->post(bind(cmd))
			load(args["-f"]);
			// if there is an "exit" in the initial file loaded, we will exit below
		}
		while ( !exit_now )
		{
			stran->post(boost::bind(&network_mobilizer::prompt, this));
			std::string cmd;
			std::getline(std::cin, cmd); // blocking call / main thread
			//if ( debug ) output << "> you typed: " << cmd << std::endl;
			if ( !cmd.find("exit") ) // necessary otherwise blocks on getline //!= std::string::npos ) // must be at the beginning of the line
			{
				exit_now = true;
				break;
			}
			else if ( !cmd.find("load") ) //!= std::string::npos ) // as above
			{
				if ( cmd.size() > 5 )
					load(cmd.substr(cmd.find("load") + 5));
			}
			else
			{
				stran->post(boost::bind(&network_mobilizer::command, this, cmd));
				// shall we post the command? is the ordering preserved? if only one thread is used, it should be
			}
		}
		std::map<std::string, end_point *>::iterator itm = end_points.begin();
		while ( itm != end_points.end() )
		{
			stran->post(boost::bind(&end_point::disc, itm->second)); // stran may not be necessary
			itm++;
		}
		work.reset(); // finish async tasks
		thread_for_io_serv_run.join();
	}

	// conn <ep> <proto> {<laddr>|*} {<lport>|*} <raddr> <rport>
	void network_mobilizer::conn_endpoint(std::vector<std::string> & argus)
	{
		if ( end_points.find(argus[1]) == end_points.end() )
		{
			end_point * ep = 0;
			try
			{
				ep = new end_point;
				if ( medeb::active ) medeb::newm(ep, sizeof(end_point),__FILE__, __LINE__);
				ep->nemo_ptr = this;
				ep->id = argus[1];
				ep->tran_proto = argus[2];
				ep->local_net_addr = argus[3];
				ep->local_port_nb = argus[4];
				ep->remote_net_addr = argus[5];
				ep->remote_port_nb = argus[6];
				end_points[ep->id] = ep;
				ep->conn();
				output << "> endpt " << ep->id << " connected\n";
			}
			catch (std::exception & e)
			{
				error << "> exception in command: " <<  e.what() << "\n";
				if ( ep ) 
				{
					end_points.erase(ep->id);
					if ( medeb::active ) medeb::delm(ep);
					delete ep;
					ep = 0;
				}
			}
		}
		else
		{
			output << "> endpt already exists\n";
		}
	}

	// serv {raw|qemu} <ep> <proto> {<laddr>|*} {<lport>|*} {<raddr>|?} {<rport>|?}
	void network_mobilizer::serv_endpoint(std::vector<std::string> & argus)
	{
		if ( end_points.find(argus[2]) == end_points.end() )
		{
			end_point * ep = 0;
			try
			{
				ep = new end_point;
				if ( medeb::active ) medeb::newm(ep, sizeof(end_point),__FILE__, __LINE__);
				ep->nemo_ptr = this;
				ep->server = true;
				ep->id = argus[1];
				ep->tran_proto = argus[2];
				ep->local_net_addr = argus[3];
				ep->local_port_nb = argus[4];
				ep->remote_net_addr = argus[5];
				ep->remote_port_nb = argus[6];
				end_points[ep->id] = ep;
				ep->serv();
				output << "> endpt " << ep->id << " listening\n";
			}
			catch (std::exception & e)
			{
				error << "> exception in command: " <<  e.what() << "\n";
				if ( ep ) 
				{
					end_points.erase(ep->id);
					if ( medeb::active ) medeb::delm(ep);
					delete ep;
					ep = 0;
				}
			}
		}
		else
		{
			output << "> endpt already exists\n";
		}
	}

	void network_mobilizer::disc_endpoint(std::vector<std::string> & argus)
	{
		if ( end_points.find(argus[1]) != end_points.end() )
		{
			end_points[argus[1]]->disc();
			output << "\n> endpt ordered to close\n";
		}
		else
		{
			output << "> endpt does not exist\n";
		}
	}

	void network_mobilizer::msg(std::vector<std::string> & argus)
	{
		if ( end_points.find(argus[1]) != end_points.end() )
		{
			std::string message;
			for ( dword i = 2; i < argus.size(); i++ )
			{
				message += argus[i];
				if ( i < argus.size() - 1 )
					message += " ";
				else
					message += "\n";
			}
			//io_serv->post(boost::bind(&end_point::msg, end_points[argus[1]], message)); // message by copy
			end_points[argus[1]]->msg(message);
		}
		else
		{
			output << "> endpt does not exist\n";
		}
	}

	void network_mobilizer::help()
	{
		output
			<< name << "\n"
			<< copyright << "\n"
			<< "  map gen <map_name> <map_type> <x_size> <y_size> <z_size> <space_step>\n"
			<< "  map {load|save} {name|<file.map>}\n"
			<< "  mbl gen <mbl_name> <duration> <nb_of_nodes> <nb_of_events> <max_speed> <max_accel>\n"
			<< "  mbl {load|save} {mbl|ns2} {name|<file.mbl>}\n"
			<< "  mbl node {<id>|all} {wic {aironet|xjack|wavelan}|txp <dBm>|margin <dBm>|cfreq <Hz>}\n"
			<< "  mbl proc <time_step>\n"
			<< "  cnn gen <cnn_name> <duration> <nb_of_nodes> <nb_of_events> <max_bw> <max_dly> <max_jit> <max_ber>\n"
			<< "  cnn {load|save} {name|<file.cnn>}\n"
			<< "  cnn {start|pause|resume|stop|status}\n"
			<< "  show {map|mbl|cnn|ept|flg}\n"
			<< "  seed {t|<seed>}\n"
			<< "  conn <ep> <proto> {<laddr>|*} {<lport>|*} <raddr> <rport>\n"
//			<< "  serv {raw|qemu} <ep> <proto> {<laddr>|*} {<lport>|*} {<raddr>|?} {<rport>|?}\n"
			<< "  disc <ep>\n"
			<< "  msg <ep> <txt>\n"
			<< "  debug {on|off}\n"
			<< "  trace {on|off}\n"
			<< "  load <command_file.txt>\n"
			<< "  exit\n";
	}

	void network_mobilizer::show(std::vector<std::string> &v)
	{
		if ( v.size() < 2 )
			output << "missing arg (see help)\n";
		else if ( v[1] == "map" )
		{
			if ( map_loaded )
				map.print(std::cout); // print map
			else
				output << "no map loaded\n";
		}
		else if ( v[1] == "mbl" )
		{
			if ( mbl_loaded )
				mobility_scenario.print(std::cout); // print mob scenario
			else
				output << "no mbl loaded\n";
		}
		else if ( v[1] == "cnn" )
		{
			if ( cnn_loaded )
				connectivity_scenario.print(std::cout); // print cnn scenario
			else
				output << "no cnn loaded\n";
		}
		else if ( v[1] == "ept" )
		{
			list(std::cout, "ept");
		}
		else if ( v[1] == "flg" )
		{
			list(std::cout, "flg");
		}
		else
			output << "unknown arg (see help)\n";
	}

	void network_mobilizer::list(std::ostream & os, std::string arg1)
	{
		if ( arg1 == "ept" )
		{
			os << "\nEndpoints\n\n";
			os << "endpoint                |type|prot|laddress       |lport|raddress       |rport|\n";
			os << "------------------------|----|----|---------------|-----|---------------|-----|\n";
			std::map<std::string, end_point *>::iterator it = end_points.begin();
			while ( it != end_points.end() )
			{
				os << std::setw(24) << std::left 
					<< (it->first.substr(0, 23)) << "|";
				if ( it->second->server )
					os << "serv|";
				else
					os << "clie|";
				if ( it->second->tran_proto == "tcp" )
				{
					os << " tcp|";
					try
					{
						os << std::setw(15) << std::right << it->second->tcp_socket->local_endpoint().address().to_string() << "|";
						os << std::setw(5) << std::right << it->second->tcp_socket->local_endpoint().port() << "|"; 
						os << std::setw(15) << std::right << it->second->tcp_socket->remote_endpoint().address().to_string() << "|"; 
						os << std::setw(5) << std::right << it->second->tcp_socket->remote_endpoint().port() << "|";
					}
					catch (std::exception & e) // listening
					{
						os << std::setw(15) << std::right << (it->second->local_net_addr /*+ "!"*/) << "|";
						os << std::setw(5) << std::right << (it->second->local_port_nb + "") << "|";
						os << std::setw(15) << std::right << "n/a" << "|"; 
						os << std::setw(5) << std::right << "n/a" << "|";
					}
				}
				if ( it->second->tran_proto == "udp" )
				{
					os << " udp|";
					if ( it->second->server )
					{
						try
						{
							os << std::setw(15) << std::right << it->second->udp_socket->local_endpoint().address().to_string() << "|";
							os << std::setw(5) << std::right << it->second->udp_socket->local_endpoint().port() << "|"; 
							os << std::setw(15) << std::right << it->second->udp_socket->remote_endpoint().address().to_string() << "|"; 
							os << std::setw(5) << std::right << it->second->udp_socket->remote_endpoint().port() << "|";
						}
						catch (std::exception & e) // unknown remote
						{
							if ( it->second->udp_remote_endpoint.address().to_string() != "127.0.0.2" )
							{
								os << std::setw(15) << std::right << it->second->udp_remote_endpoint.address().to_string() << "|"; 
								os << std::setw(5) << std::right << it->second->udp_remote_endpoint.port() << "|";
							}
							else
							{
								os << std::setw(15) << std::right << "n/a" << "|"; 
								os << std::setw(5) << std::right << "n/a" << "|";
							}
						}
					}
					else // udp client
					{
						try
						{
							os << std::setw(15) << std::right << it->second->udp_socket->local_endpoint().address().to_string() << "|";
							os << std::setw(5) << std::right << it->second->udp_socket->local_endpoint().port() << "|"; 
							os << std::setw(15) << std::right << it->second->udp_socket->remote_endpoint().address().to_string() << "|"; 
							os << std::setw(5) << std::right << it->second->udp_socket->remote_endpoint().port() << "|";
						}
						catch (std::exception & e) // unknown local?
						{
							os << std::setw(15) << std::right << (it->second->local_net_addr + "!") << "|";
							os << std::setw(5) << std::right << (it->second->local_port_nb + "") << "|";
							os << std::setw(15) << std::right << (it->second->remote_net_addr + "!") << "|";
							os << std::setw(5) << std::right << (it->second->remote_port_nb + "") << "|";
						}
					}
				}
				os << "\n";
				it++;
			}
			os << "------------------------|----|----|---------------|-----|---------------|-----|\n";
			os << "\n";
		}
		else if ( arg1 == "flg" )
		{
			os << "\nFlags\n\n";
			os <<     "flag                       |value|\n";
			os <<     "---------------------------|-----|\n";
			if ( debug )
				os << "debug                      |   on|\n";
			else
				os << "debug                      |  off|\n";
			if ( trace )
				os << "trace                      |   on|\n";
			else
				os << "trace                      |  off|\n";
			if ( map_loaded )
				os << "map_loaded                 |   on|\n";
			else
				os << "map_loaded                 |  off|\n";
			if ( mbl_loaded )
				os << "mbl_loaded                 |   on|\n";
			else
				os << "mbl_loaded                 |  off|\n";
			if ( cnn_loaded )
				os << "cnn_loaded                 |   on|\n";
			else
				os << "cnn_loaded                 |  off|\n";
			os <<     "----------------------------------\n";
		}
		else
			;
	}

	void network_mobilizer::command(std::string s)
	{
		std::istringstream iss(s);
		std::vector<std::string> argus;
		while ( iss.good() )
		{
			std::string s;
			iss >> s;
			if ( !s.empty() )
				argus.push_back(s);
		}
		if ( !argus.size() )
		{
			return;
			//if ( debug ) output << "> missing command\n";
		}
		if ( argus[0] == "" )
		{
			//if ( debug ) output << "> missing command\n";
		}
		else if ( argus[0] == "exit" )
		{
			exit_now = true;
		}
		else if ( argus[0] == "map" )
		{
			if ( argus[1] == "gen" )
				map.generate(argus);
			else if ( argus[1] == "load" )
				map.load(argus);
			else if ( argus[1] == "save" )
				map.save(argus);
			else
				;
		}
		else if ( argus[0] == "mbl" )
		{
			if ( argus[1] == "gen" )
				mobility_scenario.generate(argus);
			else if ( argus[1] == "load" )
				mobility_scenario.load(argus);
			else if ( argus[1] == "save" )
				mobility_scenario.save(argus);
			else if ( argus[1] == "node" )
				mobility_scenario.node(argus);
			else if ( argus[1] == "proc" )
			{
				if ( mobility_scenario.mobility_events.size() && mbl_loaded )
				{
					if ( debug )
						output << "nRTscheduler: starting scenario...\n";
					mobility_scenario.process(argus);
					if ( debug )
						output << "nRTscheduler: ending scenario...\n";
				}
				else
					output << "no mobility scenario loaded\n";
			}
			else
				;
		}
		else if ( argus[0] == "cnn" )
		{
			if ( argus[1] == "gen" )
				connectivity_scenario.generate(argus);
			else if ( argus[1] == "load" )
				connectivity_scenario.load(argus);
			else if ( argus[1] == "save" )
				connectivity_scenario.save(argus);
			else if ( argus[1] == "start" )
			{
				if ( connectivity_scenario.connectivity_events.size() && cnn_loaded )
				{
					if ( debug )
						output << "RTscheduler: starting scenario...\n";
					connectivity_scenario.process(); // which calls start
				}
				else
				{
						output << "no connectivity events / no scenario loaded\n";
				}
			}
			else if ( argus[1] == "pause" )
			{
				connectivity_scenario.rt_scheduler.pause();
			}
			else if ( argus[1] == "resume" )
			{
				connectivity_scenario.rt_scheduler.resume();
			}
			else if ( argus[1] == "stop" )
			{
				connectivity_scenario.rt_scheduler.stop();
			}
			else if ( argus[1] == "status" )
			{
				connectivity_scenario.rt_scheduler.status();
			}
			else //if ( argus[1] == "reset" )
			{
				output << "unknown RTscheduler command\n";
				//connectivity_scenario.rt_scheduler.reset();
			}
		}
		else if ( argus[0] == "seed" )
		{
			if ( argus[1] == "t" )
				mt19937::init_genrand(static_cast<unsigned long>(time(0)));
			else
				mt19937::init_genrand(strtoul(argus[1].c_str(), 0, 10));
		}
		else if ( argus[0] == "conn" )
		{
			conn_endpoint(argus);
		}
		else if ( argus[0] == "serv" )
		{
			serv_endpoint(argus);
		}
		else if ( argus[0] == "disc" )
		{
			disc_endpoint(argus);
		}
		else if ( argus[0] == "msg" )
		{
			msg(argus);
		}
		else if ( argus[0] == "show" )
		{
			show(argus);
		}
		else if ( argus[0] == "debug" )
		{
			if ( argus.size() == 2 )
			{
				if ( argus[1] == "on" )
				{
					debug = true;
				}
				else
				{
					debug = false;
				}
			}
		}
		else if ( argus[0] == "trace" )
		{
			if ( argus[1] == "off" )
				trace = false;
			else if ( argus[1] == "on" )
				trace = true;
			else
				output << "unknown parameter\n";
		}
		else if ( argus[0] == "help" )
		{
			help();
		}
		else
			output << "> unknown command: " << argus[0] << ", (see help for syntax)\n";
	}
}

int main(int argc, char * argv[])
{
	std::map<std::string, std::string> argvs;
	argvs["-f"] = "";  // command filename
	argvs["-d"] = "."; // working dir where logs are put
	argvs["-s"] = "t"; // seed using current time
	argvs["-c"] = "";  // batch conversion from ns2 to nemo
	if ( argc > 2 && argc % 2 == 1 )
	{
		for ( int i = 1; i < argc; i = i + 2 )
		{
			std::string k = std::string(argv[i]), v = std::string(argv[i+1]);
			if ( argvs.find(k) != argvs.end() )
				argvs.erase(k);
			argvs[k] = v; // should we check key duplicates? not done for now
		}
		std::cout 
			<< "nemo " << nemo::version << "\n"
			<< nemo::copyright << "\n\n"
			<< "waiting for commands...\n";
	}
	else
	{
		std::cout 
			<< "nemo " << nemo::version << "\n"
			<< nemo::copyright << "\n\n"
			<< "syntax: nemo -s {t|<seed>} -f <command_file> -d <log_dir>\n"
			<< "using default args...\n";
	}
	if ( argvs["-s"] == "t" )
		mt19937::init_genrand(static_cast<unsigned long>(time(0)));
	else
		mt19937::init_genrand(strtoul(argvs["-s"].c_str(), 0, 10));
	int ps = prost::random_l(6000, 60000);
	std::ostringstream oss;
	oss << ps;
	std::string sps = oss.str();
	argvs["-p"] = sps;

	// place batch conversion from ns2 to nemo here
	if ( argvs["-c"] != "" )
	{
		nemo::network_mobilizer n(argvs);
		std::string fn = argvs["-c"];
		n.mobility_scenario.load_ns2(fn);
		std::string nfn = fn.substr(0, fn.find_last_of('.')) + ".mbl";
		n.mobility_scenario.save_mbl(nfn);
		return EXIT_SUCCESS;
	}

	medeb::active = true;
	if ( medeb::active ) medeb::init();

	boost::asio::io_service io_service;
	nemo::network_mobilizer n(argvs);
	// should this be put in ctor
	n.io_serv = &io_service;
	n.stran = new boost::asio::io_service::strand(*n.io_serv);
	n.connectivity_scenario.rt_scheduler.io_serv = &io_service;

	n.process();
	//delete n.stran;

	if ( medeb::active ) medeb::dump();
	return EXIT_SUCCESS;
}
