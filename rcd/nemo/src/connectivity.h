
// nemo (network mobilizer)
// Copyright (C) 2012-2019 Damien Magoni

/*
	This file is part of nemo.

	nemo is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of
	the License, or (at your option) any later version.

	nemo is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with nemo.  If not, see <http://www.gnu.org/licenses/>.
*/

//#ifndef CONNECTIVITY_H
#if !defined(CONNECTIVITY_H)
#define CONNECTIVITY_H

#include "mobility.h"

namespace nemo
{
	typedef int8_t connectivity_status_type;
	const connectivity_status_type connectivity_status_start = 1;
	const connectivity_status_type connectivity_status_update = 2;
	const connectivity_status_type connectivity_status_stop = 3;

	class connectivity_event_type : public acted
	{
	public:
		nsec delta_time; // relative to start time
		//time_type time; // = start_time + delta_time
		dword emitting_device_id;
		dword receiving_device_id;
		connectivity_status_type connectivity_status;
		double bw, dly, jit, ber; // jit = dlv

		std::string get_status();
		void set_status(std::string s);
	};

	class connectivity_scenario_type : public actor
	{
	public:
		network_mobilizer * nemo_ptr;
		std::string name;
		rt_scheduler_type rt_scheduler;
		std::multimap<nsec, connectivity_event_type *> connectivity_events;
		std::multimap<nsec, connectivity_event_type *>::iterator itce;

		connectivity_scenario_type();
		~connectivity_scenario_type();
		void generate(std::vector<std::string> & v);
		void print(std::ostream & ost);
		void load(std::vector<std::string> & v);
		void save(std::vector<std::string> & v);
		void process(); // send orders to vnd/NEmu through RTscheduler
		void execute(const boost::system::error_code & ec, action_type * a);
	};
}

#endif // CONNECTIVITY_H
