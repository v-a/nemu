
// nemo (network mobilizer)
// Copyright (C) 2012-2019 Damien Magoni

/*
    This file is part of nemo.

    nemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    nemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with nemo.  If not, see <http://www.gnu.org/licenses/>.
*/

#if !defined(NEMO_H)
#define NEMO_H

#include "connectivity.h"
#include "end_point.h"

namespace nemo
{
	const std::string copyright = "Copyright (C) 2012-2019 Damien Magoni";
	const std::string version   = "0.2.6";

	class end_point;
	class frame;

	class network_mobilizer
	{
	public:
		std::string name; // useful if several instances are running
		bool map_loaded, mbl_loaded, cnn_loaded, debug, trace, exit_now;
		std::map<std::string, std::string> args;

		boost::asio::io_service * io_serv;
		boost::asio::io_service::strand * stran;

		map_type map;

		std::map<dword, mobile_device_type *> mobile_devices;
		std::map<dword, mobile_device_type *>::iterator itmd;

		mobility_scenario_type mobility_scenario;
		connectivity_scenario_type connectivity_scenario;

		mesap::messenger error, warning, output;

		std::map<std::string, end_point *> end_points;
		std::map<std::string, end_point *>::iterator itep;

		word port_seed, tcp_base_source_port, udp_base_source_port;

		boost::asio::ip::tcp::resolver * tcp_resolver;
		boost::asio::ip::udp::resolver * udp_resolver;

		network_mobilizer(std::map<std::string, std::string> arg);
		~network_mobilizer();
		void create_frame(frame * f);
		void destroy_frame(frame * f);
		void process();
		void conn_endpoint(std::vector<std::string> & v);
		void serv_endpoint(std::vector<std::string> & v);
		void disc_endpoint(std::vector<std::string> & v);
		void msg(std::vector<std::string> & v);
		void show(std::vector<std::string> & v);
		void list(std::ostream & os, std::string s);
		void command(std::string s);
		void load(std::string s);
		void help();
		void prompt();
	};
}

#endif
