
// nemo (network mobilizer)
// Copyright (C) 2012-2019 Damien Magoni

/*
	This file is part of nemo.

	nemo is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as
	published by the Free Software Foundation, either version 3 of
	the License, or (at your option) any later version.

	nemo is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with nemo.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nemo.h"

namespace nemo
{
	std::string connectivity_event_type::get_status()
	{
		if ( connectivity_status == connectivity_status_start )
			return "start";
		else if ( connectivity_status == connectivity_status_update )
			return "update";
		else
			return "stop";
	}

	void connectivity_event_type::set_status(std::string s)
	{
		if ( s == "start" )
			connectivity_status = connectivity_status_start;
		else if ( s == "update" )
			connectivity_status = connectivity_status_update;
		else
			connectivity_status = connectivity_status_stop;
	}

	connectivity_scenario_type::connectivity_scenario_type()
	{
		;
	}

	connectivity_scenario_type::~connectivity_scenario_type()
	{
		itce = connectivity_events.begin();
		while ( itce != connectivity_events.end() )
		{
			if ( itce->second )
			{
				if ( medeb::active ) medeb::delm(itce->second);
				delete itce->second;
			}
			itce++;
		}
		connectivity_events.clear();
	}

	// cnn gen <name> <duration> <nb_of_nodes> <nb_of_events> <max_bw> <max_dly> <max_jit> <max_ber>
	// this random generating function is quite unrealistic!
	void connectivity_scenario_type::generate(std::vector<std::string> & v) // mainly used for testing
	{
		if ( v.size() != 10 )
		{
			nemo_ptr->output << "wrong nb of parameters\n";
			return;
		}
		name = v[2]; // no spaces
		connectivity_events.clear();
		double scen_dur = strtod(v[3].c_str(), 0);

		// should be set?
		nemo_ptr->mobility_scenario.nrt_scheduler.scenario_duration_in_s = fpsec(scen_dur);

		rt_scheduler.scenario_duration_in_s = fpsec(scen_dur);
		long long ns = static_cast<long long>(scen_dur * 1E9);
		rt_scheduler.scenario_duration = nsec(ns);

		long nodes = strtol(v[4].c_str(), 0, 10);
		if ( nodes <= 0 || nodes > 1000 )
			nodes = 100; // default
		long events = strtol(v[5].c_str(), 0, 10);
		if ( events <= 0 || events > 1000 )
			events = 100; // default
		double max_bw = strtod(v[6].c_str(), 0); // bps
		if ( max_bw <= 0 || max_bw > 1E9 )
			max_bw = 54*1E6; // default
		double max_dly = strtod(v[7].c_str(), 0); // s
		if ( max_dly <= 0 || max_dly > 1 )
			max_dly = 0.001; // default
		double max_jit = strtod(v[8].c_str(), 0); // ratio (0, 1)
		if ( max_jit <= 0 || max_jit > 1.0 )
			max_jit = 0.01; // default
		double max_ber = strtod(v[9].c_str(), 0); // ratio depending on SNR=E_b/N_0
		if ( max_ber <= 0.0 || max_ber > 0.50 )
			max_ber = 0.01; // default

		// start all pairs at the beginning? yes
		for ( long i = 0; i < nodes; i++ )
		{
			for ( long j = 0; j < nodes; j++ )
			{
				connectivity_event_type * ce = new connectivity_event_type;
				long time_s = 0;//prost::random_l(0, 10 * strtol(v[3].c_str(), 0, 10)) / 10; // granularity = 1/10th second
				ce->delta_time = nsec(static_cast<long long>(1E9 * time_s)); // from start!
				ce->emitting_device_id = i + 1;
				ce->receiving_device_id = j + 1;
				if ( i == j )
					continue;
				ce->set_status("start"); // how to set a proper value! (store previous states?)
				ce->bw  = static_cast<double>(prost::random_l(1000, prost::round_to_l(max_bw)));
				ce->dly = static_cast<double>(prost::random_l(0, prost::round_to_l(max_dly))); // fn of dist?
				ce->jit = static_cast<double>(prost::random_l(0, prost::round_to_l(100 * max_jit))) / 100; // fn of intermediates/buffers?
				ce->ber = static_cast<double>(prost::random_l(0, prost::round_to_l(100 * max_ber))) / 100; // fn of dist, SNIR?
				connectivity_events.insert(std::pair<nsec, connectivity_event_type *>(ce->delta_time, ce));
			}
		}
		// update some pairs
		std::multimap<nsec, connectivity_event_type *>::iterator ceit = connectivity_events.begin();
		for ( long i = 0; i < events; i++ )
		{
			connectivity_event_type * ce = new connectivity_event_type;
			long time_s = prost::random_l(1, 10 * strtol(v[3].c_str(), 0, 10)) / 10; // granularity = 1/10th second
			ce->delta_time = nsec(static_cast<long long>(1E9 * time_s)); // from start!
			ce->emitting_device_id = prost::random_l(1, nodes);
			long mid2 = 0;
			while ( true )
			{
				mid2 = prost::random_l(1, nodes); // may take a lot of time if nodes is small!
				if ( ce->emitting_device_id != mid2 )
					break;
			}
			ce->receiving_device_id = mid2;
			ce->set_status("update");
			ce->bw  = static_cast<double>(prost::random_l(1000, prost::round_to_l(max_bw)));
			ce->dly = static_cast<double>(prost::random_l(0, prost::round_to_l(max_dly))); // fn of dist?
			ce->jit = static_cast<double>(prost::random_l(0, prost::round_to_l(100 * max_jit))) / 100; // fn of intermediates/buffers?
			ce->ber = static_cast<double>(prost::random_l(0, prost::round_to_l(100 * max_ber))) / 100; // fn of dist, SNIR?
			connectivity_events.insert(std::pair<nsec, connectivity_event_type *>(ce->delta_time, ce));
		}
		nemo_ptr->cnn_loaded = true;
	}

	// cnn load file.cnn
	void connectivity_scenario_type::load(std::vector<std::string> & v)
	{
		if ( v.size() != 3 )
		{
			nemo_ptr->output << "wrong nb of args\n";
			return;
		}
		std::string fn = v[2];
		std::ifstream f(fn.c_str());
		if ( !f )
		{
			nemo_ptr->error << "failed to open connectivity scenario file " << fn << "\n";
			return;
		}
		name = fn.substr(0, fn.find_last_of('.'));
		connectivity_events.clear();
		//nrt_scheduler.start_time = boost::chrono::high_resolution_clock::now(); // virtual epoch for nRT
		//nrt_scheduler.scenario_duration = nsec(0); // we don't know yet
		rt_scheduler.scenario_duration = nsec(0); // we don't know yet
		while ( !f.eof() )
		{
			std::string l;
			std::getline(f, l); // add error mgmt
			if ( !l.find("#") || !l.size() || !l.find("exit") )//== std::string::npos ) // exit must be at beginning of line!
				continue;
			if ( nemo_ptr->debug )
				nemo_ptr->output << "> reading: " << l << "\n";
			std::istringstream iss(l);
			std::vector<std::string> tokens;
			while ( iss.good() )
			{
				std::string s;
				iss >> s;
				if ( !s.empty() )
					tokens.push_back(s);
			}
			if ( tokens.size() == 2 && tokens[1] == "EOS" )
			{
				double secs = strtod(tokens[0].c_str(), 0);
				//nrt_scheduler.scenario_duration_in_s = fpsec(secs);
				rt_scheduler.scenario_duration_in_s = fpsec(secs);
				long long ns = static_cast<long long>(secs * 1E9);
				//nrt_scheduler.scenario_duration = nsec(ns);
				rt_scheduler.scenario_duration = nsec(ns);
			}
			else if ( tokens.size() == 8 )
			{
				connectivity_event_type * ce = new connectivity_event_type;
				ce->delta_time = nsec(static_cast<long long>(1E9 * strtod(tokens[0].c_str() , 0)));
				ce->emitting_device_id = strtoul(tokens[1].c_str(), 0, 10);
				ce->receiving_device_id = strtoul(tokens[2].c_str(), 0, 10);
				ce->set_status(tokens[3]);
				ce->bw  = strtod(tokens[4].c_str(), 0);
				ce->dly = strtod(tokens[5].c_str(), 0);
				ce->jit = strtod(tokens[6].c_str(), 0);
				ce->ber = strtod(tokens[7].c_str(), 0);
				connectivity_events.insert(std::pair<nsec, connectivity_event_type *>(ce->delta_time, ce));
			}
			else
				nemo_ptr->output << "> could not process line, skipping it\n";
		}
		f.close();
		if ( rt_scheduler.scenario_duration == nsec(0) )
		{
			nemo_ptr->output << "> could not find EOS time, using last event time\n";
			rt_scheduler.scenario_duration = nemo_ptr->mobility_scenario.mobility_events.rbegin()->first;
			rt_scheduler.scenario_duration_in_s = fpsec(static_cast<double>(rt_scheduler.scenario_duration.count()) / 1E9);
		}
		nemo_ptr->cnn_loaded = true;
	}

	void connectivity_scenario_type::print(std::ostream & ost)
	{
		// dump connectivity events
		ost << "# time_s sender_id receiver_id status bandwidth_bps delay_s jitter_% ber_%\n";
		std::multimap<nsec, connectivity_event_type *>::iterator ceit = connectivity_events.begin();
		while ( ceit != connectivity_events.end() )
		{
			connectivity_event_type * ce = ceit->second;
			ost
//				<< util::print_nsec(ceit->first) << " "
				<< util::print_nsec_as_sec(ce->delta_time) << " "
//				<< fpsec(ce->delta_time).count() << " "
				<< ce->emitting_device_id << " "
				<< ce->receiving_device_id << " "
				<< ce->get_status() << " "
				<< ce->bw << " "
				<< ce->dly << " "
				<< ce->jit << " "
				<< ce->ber << "\n";
			ceit++;
		}
		// dump EOS
		ost << util::print_nsec_as_sec(rt_scheduler.scenario_duration) << " EOS\n";
		//ost << util::print_nsec(nemo_ptr->mobility_scenario.nrt_scheduler.scenario_duration) << " EOS\n"; // pb avec le temps!!
	}

	// cnn save {name|<file.cnn>}
	void connectivity_scenario_type::save(std::vector<std::string> & v)
	{
		if ( v.size() != 3 )
		{
			nemo_ptr->output << "wrong nb of args\n";
			return;
		}
		if ( !nemo_ptr->cnn_loaded )
		{
			nemo_ptr->output << "no connectivity scenario loaded\n";
			return;
		}
		if ( v[2] == "name" )
			v[2] = name + ".cnn";
		std::ofstream f(v[2].c_str());
		if ( !f )
		{
			nemo_ptr->error << "failed to create connectivity scenario file " << v[2] << "\n";
			return;
		}
		print(f);
		f.close();
	}

	// handler called when RTsched timer expires
	void connectivity_scenario_type::execute(const boost::system::error_code & ec, action_type * a)
	{
		rt_scheduler.timer_running = false;
		if ( ec ) // useless if sync
		{
			if ( ec == boost::asio::error::operation_aborted ) // could happen? yes when pausing
				nemo_ptr->warning << "\n[cnn/execute(async_wait timer canceled)]: " << ec.value() << " = " << ec.message() << "\n";
			else
				nemo_ptr->error << "\nerror in [cnn/execute(async_wait timer issue)]: " << ec.value() << " = " << ec.message() << "\n";
			return;
		}
		if ( !a )
		{
			nemo_ptr->output << "null ptr for action * \n";
			return;
		}
		if ( a->handler == "connectivity_event" )
		{
			// send the order to NEmu at the proper time
			connectivity_event_type * ce = dynamic_cast<connectivity_event_type *>(a->doed);
			std::ostringstream oss;
			oss
				//<< "[" << a->call_time.time_since_epoch() << "] "
				<< "[" << util::print_nsec_as_sec(ce->delta_time) << "] "
				<< ce->emitting_device_id << " "
				<< ce->receiving_device_id << " "
				<< ce->get_status() << " "
				<< static_cast<unsigned long>(ce->bw) << " "
				<< ce->dly << " "
				<< ce->jit << " "
				<< ce->ber << "\n";
			oss.flush();
			std::string com = oss.str();
			nemo_ptr->itep = nemo_ptr->end_points.begin();
			while ( nemo_ptr->itep != nemo_ptr->end_points.end() )
			{
				nemo_ptr->itep->second->msg(com);
				nemo_ptr->itep++;
			}
			//if ( nemo_ptr->debug || nemo_ptr->trace )
			//	nemo_ptr->output << "\n> posted msg: " << com << "\n";
			if ( medeb::active ) medeb::delm(a);
			delete a;
			rt_scheduler.wait_till_next();
		}
		else if ( a->handler == "EOS_event" )
		{
			// send the order to nemu at the proper time
			//connectivity_event_type * ce = dynamic_cast<connectivity_event_type *>(a->doed);
			std::ostringstream oss;
			oss << "[" << util::print_nsec_as_sec(rt_scheduler.scenario_duration) << "] EOS\n";
			oss.flush();
			std::string com = oss.str();
			nemo_ptr->itep = nemo_ptr->end_points.begin();
			while ( nemo_ptr->itep != nemo_ptr->end_points.end() )
			{
				nemo_ptr->itep->second->msg(com);
				nemo_ptr->itep++;
			}
			//if ( nemo_ptr->debug || nemo_ptr->trace )
			//	nemo_ptr->output << "\n> posted msg: " << com << "\n";
			if ( medeb::active ) medeb::delm(a);
			delete a;
			//rt_scheduler.wait_till_next(); // no resched as this is the end of scenario
		}
		else
		{
			std::cout << "no handler for this action in connectivity_scenario_type\n";
		}
	}

	void connectivity_scenario_type::process()
	{
		if ( rt_scheduler.running )
		{
			nemo_ptr->output << "RTsched is already running\n";
			return;
		}
		if ( rt_scheduler.scenario_duration != nsec(0) )
			;
		else if ( nemo_ptr->mobility_scenario.nrt_scheduler.scenario_duration != nsec(0) )
			rt_scheduler.scenario_duration = nemo_ptr->mobility_scenario.nrt_scheduler.scenario_duration;
		else
		{
			nemo_ptr->output << "scenario duration is undefined\n";
			return;
		}
		rt_scheduler.destroy_actions();
		std::multimap<nsec, connectivity_event_type *>::iterator ceit = connectivity_events.begin();
		while ( ceit != connectivity_events.end() )
		{
			connectivity_event_type * ce = ceit->second;
			action_type * a = new action_type;
			if ( medeb::active ) medeb::newm(a, sizeof(action_type),__FILE__, __LINE__);
			a->description = "scheduled connectivity event";
			a->handler = "connectivity_event";
			a->delta_call_time = ceit->first;// + rt_scheduler.start_time;
			a->duration = nsec(0);
			//a->start_time = a->call_time;
			//a->stop_time = a->start_time + a->duration;
			a->doer = this;
			a->doed = ce;
			rt_scheduler.schedule(a);
			ceit++;
		}
		// schedule EOS
		action_type * a = new action_type;
		if ( medeb::active ) medeb::newm(a, sizeof(action_type),__FILE__, __LINE__);
		a->description = "scheduled EOS event";
		a->handler = "EOS_event";
		a->delta_call_time = rt_scheduler.scenario_duration;// + rt_scheduler.start_time;
		a->duration = nsec(0);
		//a->start_time = a->call_time;
		//a->stop_time = a->start_time + a->duration;
		a->doer = this;
		a->doed = 0;
		rt_scheduler.schedule(a);

		rt_scheduler.start(); // leap by leap with RT CLI
	}
}
