
// sched (schedulers)

// nemo (network mobilizer)
// Copyright (C) 2012-2014 Damien Magoni

/*
    This file is part of nemo.

    nemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of 
    the License, or (at your option) any later version.

    nemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with nemo.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "sched.h"
#include "nemo.h"

namespace nemo
{
	actor::~actor()
	{
	}

	void actor::execute(action_type * a)
	{
		;// do nothing in base class a priori
	}

	void actor::execute(const boost::system::error_code & ec, action_type * a)
	{
		;
	}

/*	nemo::mobility_event_type * acted::met_ptr()
	{
		return dynamic_cast<nemo::mobility_event_type *>(this); 
	}
*/	
	acted::~acted()
	{
	}

	// scheduler implementation

	void scheduler_type::destroy_actions()
	{
		ait = actions.begin();
		while ( ait != actions.end() )
		{
			if ( ait->second )
			{
				if ( medeb::active ) medeb::delm(ait->second);
				delete ait->second;
				//actions.erase(ait); // not necessary here
			}
			ait++;
		}
		actions.clear();
		action_id_counter = 1;
	}

	// nRT scheduler implementation

	nrt_scheduler_type::nrt_scheduler_type()//std::map<std::string, std::string> arg) :
//		nemo_ptr->error("nrts_error.log", mesap::console|mesap::logging, true),
//		*warning("nrts_warning.log", mesap::console|mesap::logging, false),
//		*output("nrts_info.log", mesap::console, false)
	{
		;
	}

	nrt_scheduler_type::~nrt_scheduler_type()
	{
		destroy_actions();
	}

	void nrt_scheduler_type::run()
	{
		// virtual epoch for nRT, never change this value afterwards!!
		start_time = boost::chrono::high_resolution_clock::now();
		current_time = start_time;
		running = true;
		while ( running && actions.size() )
		{
			// start action(s) if necessary, they are sorted by delta_call_time
			ait = actions.begin();
			action_type * current_action = ait->second;
			current_action->call_time = current_action->delta_call_time + start_time;
			if ( current_action->call_time < current_time )
			{
				nemo_ptr->error << "this action should have been executed before now\n";
				cancel(current_action->id);
			}
			else if ( current_action->call_time == current_time ) // do it
			{
				//current_action->status = started;
				current_action->doer->execute(current_action);
				if ( current_action->status == finished )
				{
					if ( medeb::active ) medeb::delm(current_action);
					delete current_action;
					actions.erase(ait);
				}
				else
					nemo_ptr->error << "this action has not properly finished\n";
			}
			else //if ( current_action->call_time > clock )
				current_time = current_action->call_time;
		}
		running = false;
		if ( actions.empty() )
			nemo_ptr->output << "nRTscheduler: EOS, no more actions\n";
		else
			nemo_ptr->output << "nRTscheduler: simulation halted, more actions left\n";
	}

	void nrt_scheduler_type::reset()
	{
		running = false;
		destroy_actions();
	}

	action_id_type nrt_scheduler_type::schedule(action_type * a) // a scheduled action may reschedule another action!
	{
		//std::cout << "scheduling action at " << std::fixed << std::setprecision(3) << a->stop_time << " s\n";

		// this below can't work anymore when using delta_call_time
		// only check when nRT is running? TODO TO CHECK
//		if ( running && ( a->start_time < current_time || a->call_time < current_time ) )
//			nemo_ptr->error << "can not schedule back in time\n";

		if ( a->duration < fpsec(0) )
			nemo_ptr->error << "can not schedule an action with a negative duration\n";
		if ( !a->doer )
			nemo_ptr->error << "can not schedule an action without an actor\n";
		//if ( a->start_time + a->duration != a->stop_time )
			//mp->err << "time vector is not correct\n"; // may happen if duration is shorter than expected
		a->status = queued;
		a->id = action_id_counter;
		action_id_counter++;
		actions.insert(std::pair<nsec, action_type *>(a->delta_call_time, a));
		return a->id;
	}

	bool nrt_scheduler_type::cancel(action_id_type id)
	{
		ait = actions.begin();
		while ( ait != actions.end() )
		{
			if ( ait->second->id == id )
			{
				if ( ait->second->status == queued || ait->second->status == started )
				{
					if ( medeb::active ) medeb::delm(ait->second);
					delete ait->second;
					actions.erase(ait);
					return true;
				}
				else
				{
					nemo_ptr->error << "can not cancel a finished/cancelled action\n";
					return false;
				}
			}
			ait++;
		}
		return false; // nothing to cancel
	}

	// RT scheduler implementation

	rt_scheduler_type::rt_scheduler_type()// :
//		nemo_ptr->error("rts_error.log", mesap::console|mesap::logging, true),
//		*warning("rts_warning.log", mesap::console|mesap::logging, false),
//		*output("rts_info.log", mesap::console, false)
	{
		timer = 0;
		timer_running = false;
		running = false;
	}

	rt_scheduler_type::~rt_scheduler_type()
	{
		if ( timer )
		{
			//timer->cancel(); // will call timer handler
			delete timer;
		}
		destroy_actions();
	}

	action_id_type rt_scheduler_type::schedule(action_type * a)
	{
		//std::cout << "scheduling action at " << std::fixed << std::setprecision(3) << a->stop_time << " s\n";
		//if ( a->start_time < current_time || a->call_time < current_time )
		//	nemo_ptr->error << "can not schedule back in time\n";
		if ( a->duration < fpsec(0) )
			nemo_ptr->error << "can not schedule an action with a negative duration\n";
		if ( !a->doer )
			nemo_ptr->error << "can not schedule an action without an actor\n";
		//if ( a->start_time + a->duration != a->stop_time )
			//mp->err << "time vector is not correct\n"; // may happen if duration is shorter than expected
		a->status = queued;
		a->id = action_id_counter;
		action_id_counter++;
		actions.insert(std::pair<nsec, action_type *>(a->delta_call_time, a));
		return a->id;
	}

	void rt_scheduler_type::wait_till_next()
	{
		if ( actions.empty() || !timer )
			return;
		action_type * a = actions.begin()->second;
		actions.erase(actions.begin());
		std::size_t handlers_cancelled = 0;
		try
		{
			time_type ct = start_time + (a->delta_call_time - offset_duration); // call time = delta time
			handlers_cancelled = timer->expires_at(ct);
		}
		catch ( std::exception & e )
		{
			nemo_ptr->error << "RTscheduler exception: " <<  e.what() << "\n";
		}
		if ( handlers_cancelled )
		{
			nemo_ptr->warning << "RTscheduler->expires_at has cancelled " << static_cast<unsigned long>(handlers_cancelled) << " handlers\n";
		}
		timer_running = true;
		timer->async_wait(boost::bind(&actor::execute, a->doer, boost::asio::placeholders::error, a));
	}

	// start in cmd should not (re)create action list!? yes it should, as actions are removed one by one
	void rt_scheduler_type::start()
	{
		running = true;
		start_time = boost::chrono::high_resolution_clock::now();
		offset_duration = nsec(0);
		timer = new timer_type(*io_serv);
		wait_till_next();
		nemo_ptr->output << "RTscheduler started\n";
	}

	void rt_scheduler_type::pause()
	{
		if ( !running )
		{
			nemo_ptr->output << "RTscheduler not running\n";
			return;
		}
		if ( timer && timer_running )
		{
			std::size_t nb_of_cancelled_async_ops = 0;
			try
			{
				nb_of_cancelled_async_ops = timer->cancel();
			}
			catch ( std::exception & e )
			{
				// boost::system::system_error thrown
				nemo_ptr->error << "RTscheduler exception: " <<  e.what() << "\n";
			}
			timer_running = false;
		}
		running = false;
	}

	void rt_scheduler_type::resume()
	{
		if ( running )
		{
			nemo_ptr->output << "RTscheduler already running\n";
			return;
		}
		if ( actions.empty() ) // EOS
		{
			nemo_ptr->output << "RTscheduler at the end of the simulation\n";
			return;
		}
		action_type * a = actions.begin()->second;
		offset_duration = a->delta_call_time; // time of 1st next action to be done = its delta time
		start_time = boost::chrono::high_resolution_clock::now(); // new start time!
		running = true;
		wait_till_next();
	}

	void rt_scheduler_type::stop()
	{
		if ( running )
			pause();
		if ( timer )
			delete timer;
		timer = 0;
		destroy_actions(); // useless as actions are removed one by one? useful if not at EOS
		nemo_ptr->output << "RTscheduler stopped\n";
	}

	void rt_scheduler_type::status()
	{
		if ( running )
		{
			nemo_ptr->output << "RTscheduler running (started or resumed)\n";
		}
		else
		{
			if ( timer )
			{
				nemo_ptr->output << "RTscheduler paused\n";
			}
			else
			{
				nemo_ptr->output << "RTscheduler stopped\n";
			}
		}
	}
}
