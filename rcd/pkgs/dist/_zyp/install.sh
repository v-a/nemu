#!/bin/sh

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0")
LOCALPATH=$(dirname "$SCRIPT")
cd $LOCALPATH

zypper update -y
zypper install -y cmake gcc-c++ make rpm-build rsync tar util-linux wget which
zypper install -y openssl-devel readline-devel libvdeplug-devel

mkdir -p /opt/boost 
cd /opt/boost
wget https://boostorg.jfrog.io/artifactory/main/release/1.75.0/source/boost_1_75_0.tar.gz
tar -xzf boost_*.tar.gz
cd $(ls -1d boost* |grep -v ".tar." |head -n1)
./bootstrap.sh
./b2 --prefix=$INST_OUTPUT_PATH --build-type=minimal --with-chrono --with-regex --with-thread --with-system variant=release link=static threading=multi install

[ -d "repo" ] && {
  ls ./repo/*.rpm &> /dev/null && zypper install -y ./repo/*.rpm || echo -n
}
