Name: nemu-virt
Summary: NEmu - Network Emulator for Mobile Universes
Version: master
Release: 1
BuildArch: x86_64

Packager: Vinent Autefage <vincent.autefage@u-bordeaux.fr>
License: LGPLv3
URL: https://gitlab.com/v-a/nemu

Requires: python3
Requires: python3-psutil
Requires: qemu-kvm
Requires: qemu-tools
Requires: squashfs
Requires: openssl
Requires: readline
Requires: vde2

Recommends: podman
Recommends: graphviz
Recommends: sshfs

%description
NEmu (Network Emulator for Mobile Universes) is a distributed virtual network environment which fully runs without any administrative rights.
It manages fleet of QEMU VMs in order to build a dynamic virtual topology.
It provides self-script (launcher and interpreter) and a python API to create and manage heterogeneous, dynamic, distributed, collaborative and mobile virtual networks.

%prep

%build

%install
rsync -a -A /nemu/_pkg/ $RPM_BUILD_ROOT/

%files
%defattr(-,root,root,-) 
/opt/nemu
/usr/bin/nemu
/usr/lib64/python__PYVER__/site-packages/nemu
%attr(0755,root,root) /opt/nemu/nemu.py


%post
which py3compile &> /dev/null && py3compile /opt/nemu/nemu
exit 0

%preun
which py3clean &> /dev/null && py3clean /opt/nemu/nemu
rm -rf /opt/nemu/nemu/__pycache__
exit 0
