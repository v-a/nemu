#!/bin/sh

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0")
LOCALPATH=$(dirname "$SCRIPT")
cd $LOCALPATH

rm -rf _build
mkdir -p _build build
cd _build

rsync -a -A ../data/ ./
sed -E -i "s/^pkgver=.*\$/pkgver=\"$DISTVER\"/g" APKBUILD
sed -E -i "s/^pkgrel=.*\$/pkgrel=\"$DISTREL\"/g" APKBUILD

adduser -D nemu
addgroup nemu abuild
echo 'nemu ALL=(ALL) NOPASSWD:ALL' >/etc/sudoers.d/nemu
sudo -u nemu abuild-keygen -a -n
chown -R nemu:nemu .
sudo -u nemu abuild -d
find /home/nemu -name '*.apk' -exec cp \{} nemu-virt-${DISTVER}-r${DISTREL}-${TAGNAME}-x86_64.apk \;
chown -R $(whoami):$(whoami) .

cd ..
find _build -name '*.apk' -exec cp \{} build/ \;
