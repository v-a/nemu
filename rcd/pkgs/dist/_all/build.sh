#!/bin/sh

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0")
LOCALPATH=$(dirname "$SCRIPT")
cd $LOCALPATH

[ -f "$LOCALPATH/preinstall.sh" ] && {
  $LOCALPATH/preinstall.sh
}

[ -f "$LOCALPATH/install.sh" ] && {
  $LOCALPATH/install.sh
}

[ -f "$LOCALPATH/postinstall.sh" ] && {
  $LOCALPATH/postinstall.sh
}

[ -f "$LOCALPATH/prebuild.sh" ] && {
  $LOCALPATH/prebuild.sh
}

export SRCDIR="$LOCALPATH/_src"
export PKGDIR="$LOCALPATH/_pkg"

[ -f "$LOCALPATH/_dist" ] && {
  export DISTNAME="$(cat $LOCALPATH/_dist |head -n1 |tr ':' '-')"
}

[ -f "$LOCALPATH/_tag" ] && {
  export TAGNAME="$(cat $LOCALPATH/_tag |head -n1)"
}

[ -f "$LOCALPATH/_rel" ] && {
  export DISTREL="$(cat $LOCALPATH/_rel |xargs)"
}

[ -f "$LOCALPATH/_flags" ] && {
  BUILD_FLAGS="$(cat $LOCALPATH/_flags |xargs)"
}

TARGET=https://gitlab.com/v-a/nemu/-/archive/master/nemu-master.tar.gz
[ -f "$LOCALPATH/_target" ] && {
  TARGET="$(cat $LOCALPATH/_target |xargs)"
}

mkdir -p $SRCDIR
mkdir -p $PKGDIR

cd $SRCDIR
wget $TARGET
tar -xzf *.tar.gz
rm -f *.tar.gz
cd $SRCDIR/nemu*/
./init.sh $BUILD_FLAGS

export DISTVER="$(cat ./nemu/__init__.py |grep "__version__" |grep "=" |cut -d = -f 2 |xargs)"

mkdir -p $PKGDIR/opt/nemu/
mkdir -p $PKGDIR/opt/nemu/rcd
cd $SRCDIR/nemu*/
cp -r nemu $PKGDIR/opt/nemu
cp nemu.py $PKGDIR/opt/nemu
cp rcd/nemu.bmp $PKGDIR/opt/nemu/rcd/
cp -r rcd/build $PKGDIR/opt/nemu/rcd/
cp -r rcd/tools $PKGDIR/opt/nemu/rcd/
cp -r rcd/vrouter $PKGDIR/opt/nemu/rcd/
cp -r rcd/vcontainer $PKGDIR/opt/nemu/rcd/

mkdir -p $PKGDIR/usr/bin/
ln -s ../../opt/nemu/nemu.py $PKGDIR/usr/bin/nemu

[ -f "$LOCALPATH/postbuild.sh" ] && {
  $LOCALPATH/postbuild.sh
}

[ -f "$LOCALPATH/prepack.sh" ] && {
  $LOCALPATH/prepack.sh
}

$LOCALPATH/pack.sh

[ -f "$LOCALPATH/postpack.sh" ] && {
  $LOCALPATH/postpack.sh
}
