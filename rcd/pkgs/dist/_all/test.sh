#!/bin/sh

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0")
LOCALPATH=$(dirname "$SCRIPT")
cd $LOCALPATH

export PKGDIR="/dist"

[ -f "$LOCALPATH/_dist" ] && {
  export DISTNAME="$(cat $LOCALPATH/_dist |head -n1 |tr ':' '-')"
}

[ -f "$LOCALPATH/_tag" ] && {
  export TAGNAME="$(cat $LOCALPATH/_tag |head -n1)"
}

export PKGDIST="$(ls -1 ${PKGDIR}/*${TAGNAME}* |head -n1)"
export PKGERR="$PKGDIST.log"

> $PKGERR

[ -f "$LOCALPATH/pretest.sh" ] && {
  $LOCALPATH/pretest.sh
}

ls /opt/nemu || echo "nemu is not available in /opt" >> $PKGERR
nemu --version || echo "nemu is not available in PATH" >> $PKGERR
test -z "$(ldd /opt/nemu/rcd/build/vnd* |grep -v -i 0x)" || echo "vnd is not healty" >> $PKGERR
test -z "$(ldd /opt/nemu/rcd/build/nemo* |grep -v -i 0x)" || echo "nemo is not healty" >> $PKGERR
python3 -c 'import nemu' || echo "nemu is not available in PYTHONPATH" >> $PKGERR

[ -f "$LOCALPATH/posttest.sh" ] && {
  $LOCALPATH/posttest.sh
}

ls /opt/nemu 2> /dev/null && echo "nemu is still installed" >> $PKGERR
ls /usr/bin/nemu 2> /dev/null && echo "nemu is still available in PATH" >> $PKGERR
python3 -c 'import nemu' 2> /dev/null && echo "nemu is still available in PYTHONPATH" >> $PKGERR

[ ! -s "$PKGERR" ] && {
  rm -f $PKGERR
}