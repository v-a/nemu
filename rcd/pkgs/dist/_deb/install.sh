#!/bin/sh

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0")
LOCALPATH=$(dirname "$SCRIPT")
cd $LOCALPATH

export DEBIAN_FRONTEND=noninteractive
apt-get --assume-yes update && apt-get --assume-yes upgrade
apt-get --assume-yes install build-essential cmake tar rsync util-linux wget
apt-get --assume-yes install libssl-dev libreadline-dev libvdeplug-dev libboost-chrono-dev libboost-regex-dev libboost-system-dev libboost-thread-dev

[ -d "repo" ] && {
  ls ./repo/*.deb &> /dev/null && apt-get --assume-yes install ./repo/*.deb || echo -n
}
