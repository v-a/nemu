#!/bin/sh

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0")
LOCALPATH=$(dirname "$SCRIPT")
cd $LOCALPATH

CTRLFILE=$(readlink -f data/DEBIAN/control)
CTRLPKG=$(cat $CTRLFILE |grep "Package:" |cut -d ':' -f 2 |xargs)
CTRLARK=$(cat $CTRLFILE |grep "Architecture:" |cut -d ':' -f 2 |xargs)
CTRLVER="${DISTVER}-${DISTREL}+${TAGNAME}"
PKGNAME="${CTRLPKG}_${CTRLVER}_${CTRLARK}"

rm -rf _build
mkdir -p _build build
cd _build

cp -r ../data ./$PKGNAME
cd $PKGNAME

rsync -a ${PKGDIR}/* ./

find . -type f ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sum
find . -type l ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum >> DEBIAN/md5sum
echo "Installed-Size: $(du -s . |xargs | cut -d ' ' -f 1)" >> DEBIAN/control
sed -E -i "s/^Version:.*\$/Version: $CTRLVER/g" DEBIAN/control

cd ..
dpkg-deb --build $PKGNAME

cd ..
find _build -name '*.deb' -exec cp \{} build/ \;
