#!/bin/sh

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0")
LOCALPATH=$(dirname "$SCRIPT")
cd $LOCALPATH

rm -rf _build
mkdir -p _build build
cd _build

rsync -a -A ../data/ ./

sed -E -i "s/^Version:.*\$/Version: $DISTVER/g" nemu-virt.spec
sed -E -i "s/^Release:.*\$/Release: $DISTREL$TAGNAME/g" nemu-virt.spec

rpmbuild --define "_topdir $PWD/rpmbuild" -bb nemu-virt.spec

cd ..
find _build -name '*.rpm' -exec cp \{} build/ \;
