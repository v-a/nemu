#!/bin/bash

# ------------------------------------------------------
# -- NEmu : The Network Emulator for Mobile Universes --
# ------------------------------------------------------

# Copyright (C) 2011-2023  Vincent Autefage

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.

#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://gitlab.com/v-a/nemu

SCRIPT=$(readlink -f "$0");
SCRIPT_PATH=$(dirname "$SCRIPT");
LOG_PATH="$SCRIPT_PATH/test.log";
POD_PATH="/tmp/nemu-containers.$USER-$UID.d";

function __get_time(){
  _GLOBAL_TIME_NOW="$(date '+%Y/%m/%d %H:%M:%S')";
}

function __do_info(){
  __get_time;
  echo -e "[Info]    $1";
  echo -e "[$_GLOBAL_TIME_NOW - Info]    $1" >> $LOG_PATH;
}

function __do_warning(){
  __get_time;
  echo -e "[Warning] $1";
  echo -e "[$_GLOBAL_TIME_NOW - Warning]  $1" >> $LOG_PATH;
}

function __do_error(){
  __get_time;
  echo -e "[Error]   $1";
  echo -e "[$_GLOBAL_TIME_NOW - Error]    $1" >> $LOG_PATH;
  __clean_share;
  __do_info "log available at $LOG_PATH";
  exit 1;
}

function __clean_share(){
  [ -n "$DIST_SHARE" ] && [ -d "$DIST_SHARE" ] && podman --root=$POD_PATH unshare rm -rf $DIST_SHARE;
}

function __init_share(){
  DIST_SHARE=$(mktemp -d --tmpdir=/tmp --suffix=.d nemu-build.XXXXXXXXXX);
}

function __test_exit(){
  if [ $? -eq 0 ]
  then
    __do_error "$1";
  fi
}

function __fail_exit(){
  if [ $? -ne 0 ]
  then
    __do_error "$1";
  fi
}

function __check_build_dependency(){
  local TEST_EXEC=$1;
  __do_info "checking for build dependency $TEST_EXEC";
  which $TEST_EXEC &>> $LOG_PATH;
  if [ $? -ne 0 ]
  then
    alias $TEST_EXEC &>> $LOG_PATH;
    __fail_exit "cannot find dependency $TEST_EXEC";
  fi
}

function check_dependencies(){
  __check_build_dependency podman;
  __check_build_dependency rsync;
}

function from_dist(){
  local FROM_HOME="$(readlink -f $SCRIPT_PATH/dist/$1)";
  if [ -e "$FROM_HOME/_from" ]
  then
    for dist in $(cat $FROM_HOME/_from 2> /dev/null |xargs)
    do
      from_dist $dist;
    done
  fi
  __do_info "importing files from dist $1";
  rsync -a $FROM_HOME/ $DIST_SHARE/;
  __fail_exit "cannot import files from dist $1 into $DIST_SHARE";
  echo "$1" > $DIST_SHARE/_dist;
}

function pull_dist(){
  if ! [ -d $DIST_HOME ]
  then
    __do_error "cannot find vnd directory $DIST_HOME";
  fi
  podman --root=$POD_PATH image exists $DIST_IMG;
  if [ $? -ne 0 ]
  then
    __do_info "pulling container $1";
    podman --root=$POD_PATH pull $DIST_IMG 2>&1 | tee -a $LOG_PATH;
    __fail_exit "cannot pull container $1";
  fi
}

function test_pkg(){
  __do_info "testing package for $1";
  DIST_HOME="$(readlink -f $SCRIPT_PATH/dist/$1)";
  DIST_IMG="docker.io/$(echo $1 |tr '@' '/')";
  DIST_BUILD="$SCRIPT_PATH/build";
  if ! [ -d $DIST_HOME ]
  then
    __do_error "cannot find dist directory $DIST_HOME";
  fi
  __init_share;
  pull_dist $1;
  from_dist $1;
  mkdir -p $DIST_BUILD;
  podman --root=$POD_PATH run --rm -it -v $DIST_SHARE:/nemu -v $DIST_BUILD:/dist $DIST_IMG /nemu/test.sh >&1 | tee -a $LOG_PATH;
  __fail_exit "cannot test package for $1";
  __clean_share;
}

echo -n > $LOG_PATH;

check_dependencies;

[ -n "$*" ] && DIST_LIST="$*" || DIST_LIST="$(ls -1 $SCRIPT_PATH/dist/ |grep -v -E '^_.*$')";

for DIST_ENTRY in $DIST_LIST
do
  for DIST_NAME in $SCRIPT_PATH/dist/$DIST_ENTRY*
  do
    test_pkg $(basename $DIST_NAME);
  done
done
__do_info "log available at $LOG_PATH";
